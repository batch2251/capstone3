import { Provider } from 'react-redux'
import { Routes, Route } from "react-router-dom";
import { Chart as ChartJS, CategoryScale, LinearScale, PointElement, LineElement, Title, Tooltip, Legend, ArcElement } from 'chart.js';

import {Container} from 'react-bootstrap';

import AppNavbar from '@/components/partials/AppNavbar';
import AppFooter from '@/components/partials/AppFooter';

import Home from '@/components/pages/Home';
import Register from '@/components/pages/Register';

import Login from '@/components/pages/Login';
import Logout from '@/components/pages/Logout';

import Effects from '@/components/sections/Effects';
import GroceryList from '@/components/pages/GroceryList';
import GroceryListShared from '@/components/pages/GroceryListShared';
import Loading from '@/components/sections/Loading';
import ToastMessage from '@/components/sections/Toast';

import History from '@/components/pages/user/History'
import HistoryInfo from '@/components/pages/user/HistoryInfo'
import Profile from '@/components/pages/user/Profile'
import UserPromos from '@/components/pages/user/Promos'
import ProductDetails from '@/components/pages/ProductDetails'

import Dashboard from '@/components/pages/admin/Dashboard'
import ProductAdd from '@/components/pages/admin/products/Add'
import ProductList from '@/components/pages/admin/products/List'
import ProductEdit from '@/components/pages/admin/products/Edit'
import OrderList from '@/components/pages/admin/orders/List'
import OrderDetails from '@/components/pages/admin/orders/Details'

import UserAdd from '@/components/pages/admin/users/Add'
import UserList from '@/components/pages/admin/users/List'
import UserEdit from '@/components/pages/admin/users/Edit'

import E404 from '@/components/pages/E404';
import NotLogged from '@/components/pages/NotLogged';
import NotAllowed from '@/components/pages/NotAllowed';
import Suggested from '@/components/pages/Suggested';

import store from '@/store/store'
import '@/App.css';


import { library } from '@fortawesome/fontawesome-svg-core'

import { faHome, faSearch, faLightbulb, faGear, faShareNodes, faStar, faUser, faUsers, faCartPlus, faCartShopping, faPen, faArrowRightToBracket, faUserPlus, faCirclePlus, faTrash, faListCheck, faStore, faCircleCheck, faFloppyDisk, faCircleXmark } from '@fortawesome/free-solid-svg-icons'
library.add(faHome, faSearch, faLightbulb, faGear, faShareNodes, faStar, faUser, faUsers, faCartPlus, faCartShopping, faPen, faArrowRightToBracket, faUserPlus, faCirclePlus, faTrash, faListCheck, faStore, faCircleCheck, faFloppyDisk, faCircleXmark)


ChartJS.register( CategoryScale, LinearScale, PointElement, LineElement, Title, Tooltip, Legend, ArcElement );

function App() {
  return (
    <>
      <Provider store={store}>
        <AppNavbar  />
        <Container fluid id="main">
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/register" element={<Register />} />
              <Route path="/login" element={<Login />} />
              <Route path="/logout" element={<Logout/>} />

              <Route path="/grocery-list" element={<GroceryList />} />

              <Route path="/my/history" element={<History />} />
              <Route path="/my/history/:id" element={<HistoryInfo />} />
              <Route path="/my/profile" element={<Profile />} />
              <Route path="/promos/:id" element={<UserPromos />} />
              <Route path="/details/:id" element={<ProductDetails />} />

              <Route path="/admin/dashboard" element={<Dashboard />} />
              <Route path="/admin/products/add" element={<ProductAdd />} />
              <Route path="/admin/products/list" element={<ProductList />} />
              <Route path="/admin/product/:id" element={<ProductEdit />} />

              <Route path="/admin/orders/daily/:date" element={<OrderList />} />
              <Route path="/admin/order/:id" element={<OrderDetails />} />


              <Route path="/admin/users/list" element={<UserList />} />
              <Route path="/admin/user/:id" element={<UserEdit />} />
              <Route path="/admin/users/add" element={<UserAdd />} />

              <Route path="*" element={<E404 />} />
              <Route path="/not-logged-in" element={<NotLogged />} />
              <Route path="/not-allowed" element={<NotAllowed />} />
              <Route path="/suggested" element={<Suggested />} />
              <Route path="/shared/:id" element={<GroceryListShared />} />

            </Routes>
            <Effects />
            <Loading />
            <ToastMessage />
         </Container>
         <AppFooter />
      </Provider>
    </>
  );
}

export default App;
