import { configureStore } from '@reduxjs/toolkit'
import counterReducer from '@/store/slice/counterSlice'
import userReducer from '@/store/slice/userSlice'
import listReducer from '@/store/slice/listSlice'
import listSharedReducer from '@/store/slice/listSharedSlice'
import filterReducer from '@/store/slice/filterSlice'
import loadingReducer from '@/store/slice/loadingSlice'
import toastReducer from '@/store/slice/toastSlice'

export default configureStore({
  reducer: {
  	counter: counterReducer,
  	user: userReducer,
    list: listReducer,
    loading: loadingReducer,
    toast: toastReducer,
    filter: filterReducer,
    listShared: listSharedReducer
  },
})
