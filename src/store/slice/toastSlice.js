import { createSlice } from '@reduxjs/toolkit'

export const toastSlice = createSlice({
  name: 'toast',
  initialState: {
    status: false,
    message: '...'
  },

  reducers: {
    toastSet: (state, action) => {
      state.status = action.payload.status
      state.message = action.payload.message
    },
  },
})

export const { toastSet } = toastSlice.actions
export default toastSlice.reducer
