import mongoose from 'mongoose';
import { createSlice } from '@reduxjs/toolkit'

export const listSlice = createSlice({
  name: 'list',
  initialState: {
    data: ( localStorage.getItem('listahan_list') !== null ) ?  JSON.parse(localStorage.getItem('listahan_list')).data : []
  },

  reducers: {
    listInsert: (state, action) => {
      state.data.push(action.payload)
      // save to localstorage
      localStorage.setItem('listahan_list',  JSON.stringify({data: state.data}) );
    },

    listRemove: (state, action) => {
      state.data.splice(action.payload, 1)
      // save to localstorage
      localStorage.setItem('listahan_list',  JSON.stringify({data: state.data}) );
    },

    listUpdateByIndex: (state, action) => {
      //console.log(action.payload)
      state.data[action.payload.index].productId = (action.payload.newData.productId) ? mongoose.Types.ObjectId(action.payload.newData.productId) : null
      state.data[action.payload.index].name = action.payload.newData.name
      state.data[action.payload.index].price = action.payload.newData.price
      state.data[action.payload.index].quantity = action.payload.newData.quantity
      state.data[action.payload.index].disableName = action.payload.newData.disableName
      state.data[action.payload.index].disablePrice = action.payload.newData.disablePrice
      state.data[action.payload.index].bought = action.payload.newData.bought
      state.data[action.payload.index].index = action.payload.newData.index
      // save to localstorage
      localStorage.setItem('listahan_list',  JSON.stringify({data: state.data}) );
    },

    listPurge: (state) => {
      localStorage.removeItem('listahan_list');
      state.data = []
    }
  },
})

export const { listInsert, listRemove, listUpdateByIndex, listPurge } = listSlice.actions
export default listSlice.reducer
