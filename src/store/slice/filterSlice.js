import { createSlice } from '@reduxjs/toolkit'

export const filterSlice = createSlice({
  name: 'filter',
  initialState: {
    month: null,
    year: null
  },

  reducers: {
    filterSet: (state, action) => {
      state.month = action.payload.month
      state.year = action.payload.year
    },
  },
})

export const { filterSet } = filterSlice.actions
export default filterSlice.reducer
