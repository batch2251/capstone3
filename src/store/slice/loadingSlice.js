import { createSlice } from '@reduxjs/toolkit'

export const loadingSlice = createSlice({
  name: 'loading',
  initialState: {
    status: false,
    message: 'Loading'
  },

  reducers: {
    loadingSet: (state, action) => {
      state.status = action.payload.status
      state.message = action.payload.message
    },
  },
})

export const { loadingSet } = loadingSlice.actions
export default loadingSlice.reducer
