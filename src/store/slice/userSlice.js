import { createSlice } from '@reduxjs/toolkit'

export const userSlice = createSlice({
  name: 'user',

  initialState: {
    fname: null,
    lname: null,
    email: null,
    token: null,
    id: null,
    role: null
  },

  reducers: {
    userSet: (state, action) => {
      state.fname = action.payload.fname
      state.lname = action.payload.lname
      state.email = action.payload.email
      state.token = action.payload.access
      state.id    = action.payload.id
      state.role  = action.payload.role
    },

    userGet: (state) => {
      //console.log(action)
      let storage = JSON.parse(localStorage.getItem('listahan_user'))
      if (storage !== null) {
        state.fname = storage.firstName
        state.lname = storage.lastName
        state.email = storage.email
        state.token = storage.access
        state.id    = storage.id
        state.role  = storage.role
      }
    },

    userPurge: (state) => {
        localStorage.removeItem('listahan_user')
        state.fname = null
        state.lname = null
        state.email = null
        state.token = null
        state.id    = null
        state.role  = null
    },
  },
})

export const { userSet, userGet, userPurge } = userSlice.actions
export default userSlice.reducer
