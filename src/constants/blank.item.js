module.exports = {
  productId: undefined,
	index: '',
	name: '',
	quantity: 1,
	price: 0,
	disablePrice: false,
	disableName: false,
	bought: false
}
