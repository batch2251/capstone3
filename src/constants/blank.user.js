module.exports =  {
  firstName: "",
  lastName: "",
  email: "",
  mobileNo: "",
  password: "",
  password2: "",
  address: {
      street1: "",
      street2: "",
      city: "",
      province: "",
      postalCode: "",
      country: "",
  },
  role: 2,
  isActive: true
}
