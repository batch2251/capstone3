//import React, { useState } from 'react';
import {Modal, Button} from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux'
import { toastSet } from '@/store/slice/toastSlice'

function ToastMessage() {
  const toast = useSelector((state) => state.toast)
  const dispatch = useDispatch()

  //const [toastToggle, setToast] = useState(true);
  const toastToggler = () => {
    dispatch( toastSet({ status: false, message: '...' }) )
  }

  return (
    <>
      <Modal show={toast.status} onHide={toastToggler} size="lg">
        <Modal.Body>
          {toast.message}
          <hr />
          <div align="center">
            <Button variant="primary" onClick={toastToggler}>
              OK
            </Button>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default ToastMessage;
