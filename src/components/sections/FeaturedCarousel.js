import { Carousel } from 'react-bootstrap';
import { Link } from 'react-router-dom'

import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux'
import { loadingSet } from '@/store/slice/loadingSlice'

import { API_PUBLIC } from "@/helpers/api-public.js"

export default function FeaturedCarousel() {
    const [featured, setFeatured] = useState([])
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch( loadingSet({ status: true, message: 'Fetching Featured...' }) )

        API_PUBLIC(null, '/api/featured', 'get')
          .then( (response) => {
            setFeatured(JSON.parse(response))
            dispatch( loadingSet({ status: false, message: '' }) )
        })          
    }, [dispatch]);


    const RenderCarouselItems = () =>  {
        if(featured.length < 1) return
          let items = [];

          featured.map( (item, index) => {
            items.push(
  		      <Carousel.Item key={index}>
  		      	<Link to={`/promos/${item._id}`}>
  		        <img className="d-block w-100" src={item.imageUrl} alt={item.name} />
  		        <Carousel.Caption>
  		          <h3>{item.name}</h3>
  		        </Carousel.Caption>
  		        </Link>
  		      </Carousel.Item>
            )

            return index
          })
                
          return (
            <>
              <Carousel fade indicators={false} id="featuredCarousel">{items}</Carousel>
            </>
          )      
    }   


    return (
	<RenderCarouselItems />
    )
}
