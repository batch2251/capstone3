import { Modal, Spinner } from 'react-bootstrap';
import { useSelector } from 'react-redux'
//import { loadingSet } from '@/store/slice/loadingSlice'

function Loading() {
  const loading = useSelector((state) => state.loading)

  return (
    <>
      <Modal show={loading.status} centered size="sm" backdrop="static">
        <Modal.Body className="modalMessage">
          <Spinner animation="border" role="status" size="sm" variant="success" /> {loading.message}
        </Modal.Body>
      </Modal>
    </>
  );
}

export default Loading;
