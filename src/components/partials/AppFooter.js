import { Container } from 'react-bootstrap';
//import { Link } from 'react-router-dom';

export default function Footer() {
    const d = new Date();

    return (
        <>
          <Container fluid className="footer">
            <div align="center">
              Copyright &copy; { d.getFullYear() }. All Rights Reserved.
            </div>
          </Container>
        </>
    )
}
