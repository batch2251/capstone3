import { useRef, useEffect, useState } from 'react'
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { useSelector, useDispatch } from 'react-redux'
import { userGet } from '@/store/slice/userSlice'

import { Nav, Navbar, NavDropdown, Container, Offcanvas } from 'react-bootstrap';
import Logo from '@/images/logo.png';

export default function AppNavbar() {
  const user = useSelector((state) => state.user)
  const dispatch = useDispatch()

  let oldScrollY = 0;
  const [direction, setDirection] = useState('up');
  const controlDirection = () => {
    if(window.scrollY > oldScrollY) {
         document.body.classList.add('scrolledDown')
        setDirection('down');
    } else {
        document.body.classList.remove('scrolledDown')
        setDirection('up');
    }
    oldScrollY = window.scrollY;
  }

  useEffect(() => {
      window.addEventListener('scroll', controlDirection);
      return () => {
          window.removeEventListener('scroll', controlDirection);
      };

  },[]);


  useEffect(() => {
    dispatch( userGet() )
  }, [user, dispatch]);

  const offCanvasRef = useRef();
  const closeOffCanvas = () => offCanvasRef.current.backdrop.click();

  return (
    <>
      <Container fluid>
        <Navbar
          id="main-nav"
          bg={(user.role === 0) ? 'light' : 'dark'}
          variant={(user.role === 0) ? 'light' : 'dark'}
          expand={user.role === 0 ? 'lg' : ''}
          className="mb-3"
          collapseOnSelect="true"
          fixed="top"
        >
            <Navbar.Brand as={Link} to="/"><img src={Logo} width="160" alt="Logo" /></Navbar.Brand>
            {
              (user.role === 0)  ?
                <>
                  <Navbar.Toggle aria-controls="adminNav" />
                  <Navbar.Collapse id="adminNav"  className="justify-content-end">
                    <span id="nameAvatar">{user.fname.substring(1, 0)}{user.lname.substring(1, 0)}</span>
                    <Nav>
                      <Nav.Link as={Link} to="/admin/dashboard">
                        <FontAwesomeIcon icon="home" /> Dashboard
                      </Nav.Link>

                      <NavDropdown title={<span><FontAwesomeIcon icon="users" /> Users</span>}>
                        <NavDropdown.Item as={Link} to="/admin/users/list">
                          <FontAwesomeIcon icon="users" /> All Users
                        </NavDropdown.Item>

                        <NavDropdown.Divider />
                        <NavDropdown.Item as={Link} to="/admin/users/add">
                          <FontAwesomeIcon icon="user-plus" /> Add User
                        </NavDropdown.Item>
                      </NavDropdown>

                      <NavDropdown title={<span><FontAwesomeIcon icon="cart-shopping" /> Products</span>}>
                        <NavDropdown.Item as={Link} to="/admin/products/list">
                          <FontAwesomeIcon icon="cart-shopping" /> All Products
                        </NavDropdown.Item>
                        <NavDropdown.Divider />
                        <NavDropdown.Item as={Link} to="/admin/products/add">
                          <FontAwesomeIcon icon="cart-plus" />  Add Product
                        </NavDropdown.Item>
                      </NavDropdown>

                      <Nav.Link as={Link} to="/logout">
                        <FontAwesomeIcon icon="arrow-right-to-bracket" /> Logout
                      </Nav.Link>
                    </Nav>
                  </Navbar.Collapse>
                </>
              :
                <>
                <Navbar.Toggle aria-controls="offcanvasNavbar-expand" id="navToggle" />
                <Navbar.Offcanvas id="offcanvasNavbar-expand" aria-labelledby="offcanvasNavbarLabel-expand" placement="end" ref={offCanvasRef} >
                  <Offcanvas.Header closeButton>
                    {
                      (user.token) ?
                      <Offcanvas.Title id="offcanvasNavbarLabel-expand">
                        <div id="nameAvatar">{user.fname.substring(1, 0)}{user.lname.substring(1, 0)}</div>
                        {user.fname} {user.lname}
                      </Offcanvas.Title>
                      :
                      <Offcanvas.Title id="offcanvasNavbarLabel-expand"></Offcanvas.Title>
                    }
                  </Offcanvas.Header>
                  <hr />
                  <Offcanvas.Body>
                    <Nav className="justify-content-end flex-grow-1 pe-3">
                      <Nav.Link as={Link} to="/" onClick={closeOffCanvas}>
                        <FontAwesomeIcon icon="home" /> Home
                      </Nav.Link>
                      
                      <Nav.Link as={Link} to="/grocery-list" onClick={closeOffCanvas}>
                        <FontAwesomeIcon color="success" icon="cart-shopping" /> Create A Grocery List
                      </Nav.Link>  

                      <Nav.Link as={Link} to="/suggested" onClick={closeOffCanvas}>
                        <FontAwesomeIcon color="success" icon="lightbulb" /> Suggested Products
                      </Nav.Link>                        

                      <Nav.Link as={Link} to="/my/history" onClick={closeOffCanvas}>
                        <FontAwesomeIcon icon="list-check" /> Shopping History
                      </Nav.Link>

                      {
                        (user.token) ?
                          <>
                          <Nav.Link as={Link} to="/my/profile" onClick={closeOffCanvas}> 
                            <FontAwesomeIcon icon="user" /> Profile
                          </Nav.Link>
                          <Nav.Link as={Link} to="/logout" onClick={closeOffCanvas}>
                            <FontAwesomeIcon icon="arrow-right-to-bracket" />  Logout
                          </Nav.Link>
                          </>
                          :
                          <>
                          <Nav.Link as={Link} to="/login" onClick={closeOffCanvas}>
                            <FontAwesomeIcon icon="arrow-right-to-bracket" /> Login
                          </Nav.Link>
                          <Nav.Link as={Link} to="/register" onClick={closeOffCanvas}>
                            <FontAwesomeIcon icon="user-plus" /> Create an Account
                            </Nav.Link>
                          </>
                      }

                    </Nav>
    {/*                <Form className="d-flex">
                      <Form.Control
                        type="search"
                        placeholder="Search"
                        className="me-2"
                        aria-label="Search"
                      />
                      <Button variant="outline-success">Search</Button>
                    </Form>*/}
                  </Offcanvas.Body>
                </Navbar.Offcanvas>
                </>
            }
        </Navbar>
      </Container>
    </>
  );
}
