import AppNavbar from '@/components/partials/AppNavbar';
import Home from '@/components/pages/Home';
import Register from '@/components/pages/Register'; 
import Login from '@/components/pages/Login';
import Logout from '@/components/pages/Logout';

export default {
  AppNavbar,
  Home,
  Register,
  Login,
  Logout
}
