import { Container, Row, Col, Button } from 'react-bootstrap';
import { Link, Navigate, useNavigate, useSearchParams  } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React, { useState, useEffect } from 'react';

import { useSelector, useDispatch } from 'react-redux'
import { loadingSet } from '@/store/slice/loadingSlice'

import { AUTH_CHECK } from "@/helpers/auth-check.js"
import { API_PRIVATE } from "@/helpers/api-private.js"

import { Line } from 'react-chartjs-2';

import Months from '@/constants/constant.months'

export default function AdminLineChart(props) {
    const filter = useSelector((state) => state.filter)

    const isLoggedIn = AUTH_CHECK()
    const [data, setData] = useState([])
    const dispatch = useDispatch()
    const navigate = useNavigate()

    useEffect(() => {
        dispatch( loadingSet({ status: true, message: 'Fetching Grocery Lists...' }) )

        let month = filter.month
        let year = filter.year
        let params =  (month, year) ? '?' + new URLSearchParams({month: Months.indexOf(filter.month), year: filter.year}).toString() : ''

        API_PRIVATE(null, '/api/order/list/daily' + params, 'get')
          .then( (response) => {
            setData(JSON.parse(response))
            dispatch( loadingSet({ status: false, message: '' }) )
        })
    }, [filter]);

    function RenderChart () {
      const labels = []
      const chartItems = []
      const chartItemId = []

      data.map( (item) => {
        labels.push( item.date)
        chartItems.push(item.count)
        chartItemId.push(item.date)
      })

      const chartData = {
        labels,
        datasets: [
          {
            label: 'Saved Grocery List',
            data: chartItems,
            borderColor: 'rgb(50, 83, 71)',
            backgroundColor: 'rgb(139, 30, 63)',
            pointRadius: 6,
            hoverBorderWidth: 12,
          },
        ],
      };

      const options = {
        onClick (evt, item) {
          if (!item[0]) return
          navigate(`/admin/orders/daily/${chartItemId[item[0].index]}`)
        },        
        responsive: true,
        plugins: {
          legend: {
            display: false,
          },
        },
      }

      return (
        <>
          <Line options={options} data={chartData} id="fefe" />
        </>
      )
    }

    const RenderMonths = () => {
        return (
          Months.map( (item, index) => {
            return (
              <option value={item.value} key={index}>{item.name}</option>
            )
          })
        )
    }   

    const RenderYear = () =>  {
        let date = new Date()
        let year = date.getFullYear()      
        let items = [];
        for (let page = year; page >= (year - 20); page--) {
          items.push(
            <option key={page} value={page}>{page}</option>,
          );
        }                  
        return (
          <>
            {items}
          </>
        )      
    }    

    return (
  	<>
	<h3 align="center">{ filter.month } { filter.year } USER GROCERY LISTS</h3>
	<RenderChart />
  	</>
    )
}
