import { useNavigate } from 'react-router-dom'
import React, { useState, useEffect } from 'react';

import { useSelector, useDispatch } from 'react-redux'
import { loadingSet } from '@/store/slice/loadingSlice'

import { AUTH_CHECK } from "@/helpers/auth-check.js"
import { API_PRIVATE } from "@/helpers/api-private.js"

import { Line } from 'react-chartjs-2';

import Months from '@/constants/constant.months'

export default function UserLineChart(props) {
    const filter = useSelector((state) => state.filter)
    const isLoggedIn = AUTH_CHECK()
    const [data, setData] = useState([])
    const dispatch = useDispatch()
    const navigate = useNavigate()

    useEffect(() => {
        dispatch( loadingSet({ status: true, message: 'Fetching Profile...' }) )

        let month = filter.month
        let year = filter.year
        let params =  (month, year) ? '?' + new URLSearchParams({month: Months.indexOf(filter.month), year: filter.year}).toString() : ''

        API_PRIVATE(null, '/api/users/orders' + params, 'get')
          .then( (response) => {
            setData(JSON.parse(response))
            dispatch( loadingSet({ status: false, message: '' }) )
        })
    }, [filter]);

    function RenderChart () {
      const labels = []
      const chartItems = []
      const chartItemId = []

      data.map( (item) => {
        labels.push( new Date(item.purchasedOn).toLocaleDateString("en-US") )
        chartItems.push(item.subtotal)
        chartItemId.push(item._id)
      })

      const chartData = {
        labels,
        datasets: [
          {
            label: 'Total Expense',
            data: chartItems,
            borderColor: 'rgb(50, 83, 71)',
            backgroundColor: 'rgb(139, 30, 63)',
            pointRadius: 10,
            hoverBorderWidth: 12,
          },
        ],
      };

      const options = {
        onClick (evt, item) {
          if (!item[0]) return
          navigate(`/my/history/${chartItemId[item[0].index]}`)
        },        
        responsive: true,
        label: 'FEFENESS',
        plugins: {
          legend: {
            display: false,
          },
        },
      }

      return (
        <>
          <Line options={options} data={chartData} />
        </>
      )
    }

    return (
  <>
    <h4 align="center">{ filter.month } { filter.year } Expenses</h4>   
    <RenderChart />
  </>
    )
}
