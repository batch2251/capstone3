import { useNavigate } from 'react-router-dom'
import React, { useState, useEffect } from 'react';

import { useSelector, useDispatch } from 'react-redux'
import { loadingSet } from '@/store/slice/loadingSlice'

import { AUTH_CHECK } from "@/helpers/auth-check.js"
import { API_PRIVATE } from "@/helpers/api-private.js"

import { Doughnut, ArcElement } from "react-chartjs-2";

import Colors from "@/constants/constant.colors.js"
import Months from '@/constants/constant.months'

export default function UserDoughnutChart(props) {
    const filter = useSelector((state) => state.filter)
    const isLoggedIn = AUTH_CHECK()
    const [data, setData] = useState([ 
      { 
        result: [{item: 'No data', expense: 0}] 
      } 
    ])

    const dispatch = useDispatch()
    const navigate = useNavigate()

    useEffect(() => {
        dispatch( loadingSet({ status: true, message: 'Fetching Profile...' }) )

        let month = filter.month
        let year = filter.year
        let params =  (month, year) ? '?' + new URLSearchParams({month: Months.indexOf(filter.month), year: filter.year}).toString() : ''        

        API_PRIVATE(null, '/api/order/list/itemized' + params, 'get')
          .then( (response) => {
            let r = JSON.parse(response)
            let result = (r.length > 0) ? r : [{ result: {} }]
            setData(result)            
            dispatch( loadingSet({ status: false, message: '' }) )
        })
    }, [filter]);

    function RenderChart () {
      const labels = []
      const chartItems = []

      if ( data[0].result.length > 0 ) {
        data[0].result.map( (item) => {
          labels.push( item.name )
          chartItems.push(item.expense)
        })
      } else {
          labels.push( 'No data' )
          chartItems.push(0.00000001)          
      }      

      const chartData = {
      labels: labels,
      datasets: [{
        label: 'Amount Spent',
          data: chartItems,
        backgroundColor: Colors,          
      }]        
      
      }

      const options = {
        responsive: true,
        plugins: {
          legend: {
            display: false,
          },
        },
      }

      return (
        <>
          <h4 align="center">{filter.month} {filter.year} User Top 10 Items</h4>
          <Doughnut options={options} data={chartData} datasetIdKey="fufu" />
        </>
      )
    }

    return (
  <>
    <RenderChart />
  </>
    )
}
