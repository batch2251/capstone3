import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { listInsert } from '@/store/slice/listSlice'
import { loadingSet } from '@/store/slice/loadingSlice'
import { toastSet } from '@/store/slice/toastSlice'

import { Container, Row, Col, Card, Button} from 'react-bootstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { API_PUBLIC } from "@/helpers/api-public.js"

export default function ProductDetails(props) {
    const PRODUCT_ID = useParams();

    const dispatch = useDispatch()    

    const [data, setData] = useState({
    	result: {
    		name: '',
    		imageUrl: '',
    		description: ''
    	}
    })

  const addToGroceryList = async (suggested = null) => {
      let inserted = await dispatch( listInsert({
          productId: data.result._id,
          name: data.result.name,
          price: data.result.price,
          quantity: 1
      	})
      )

      if (inserted) {
		dispatch( toastSet({ status: true, message: `${data.result.name} added to your grocery list!` }) )
      }
  }    

    useEffect( () => {
          dispatch( loadingSet({ status: true, message: 'Getting Product Details...' }) )
          API_PUBLIC(null, `/api/products/${PRODUCT_ID.id}`, 'get')
            .then( (response) => {
              setData( JSON.parse(response) )
              dispatch( loadingSet({ status: false, message: '' }) )
            })
    }, [PRODUCT_ID, dispatch]);

    return (
  <>
  <Container>
      <Card className="brownCard flexible home">
        <Card.Body>
          <Card.Title><h1 align="center">{data.result.name}</h1></Card.Title>

          <Row>
          	<Col md="4" sm="12"><img src={data.result.imageUrl} alt={data.result.name} className="mb-5" />	</Col>
          	<Col md="8" sm="12">
          		{data.result.description}
          		<hr />
          		<Button variant="success"  onClick={addToGroceryList} >
          			<FontAwesomeIcon icon="cart-shopping"/> Add Item to my Grocery List
          		</Button>
          	</Col>
          </Row>
         
          <hr />
        </Card.Body>
      </Card>
  </Container>
  </>
    )
}
