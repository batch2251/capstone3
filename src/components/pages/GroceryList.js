import React, { useState, useEffect } from 'react';

import { useSelector, useDispatch } from 'react-redux'
import { listInsert, listUpdateByIndex, listRemove, listPurge } from '@/store/slice/listSlice'
import { userGet } from '@/store/slice/userSlice'
import { loadingSet } from '@/store/slice/loadingSlice'
import { toastSet } from '@/store/slice/toastSlice'

import { API_PRIVATE } from "@/helpers/api-private.js"

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Card, Button, Alert, Modal, Form, FloatingLabel, Dropdown, DropdownButton, ButtonGroup } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import blankItem from '@/constants/blank.item'

export default function GroceryList() {
  const [errorSave, setErrorSave] = useState({ status: false, message: ''})
  // total
  const [total, setTotal] = useState(0)
  // done shopping
  const [allDone, setAllDone] = useState([false])
  useEffect( () => {
    if(allDone.length === 0) {
      setModalDone(true)
    }
  }, [allDone]);

  const[item, setItem] = useState(blankItem);
  const [button, setButton] = useState('default')
  const [modal, setModal] = useState(false);
  const handleClose = () => { setModal(false) };
  const [modalDone, setModalDone] = useState(false);
  const handleDoneClose = () => { setModalDone(false) };

  const list = useSelector((state) => state.list)
  const user = useSelector((state) => state.user)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch( userGet() )
  }, [user]);

  useEffect( () => {
    let itemTotal = 0
    let isDone = []
    list.data.map((element) => {
      if (element.bought) {
        let itemSubTotal = ( !isNaN(element.quantity) ) ? parseFloat(element.price)*parseFloat(element.quantity) : element.price
        setTotal( itemTotal = itemTotal + parseFloat(itemSubTotal) )
      } else {
        isDone.push('false')
      }
      setAllDone(isDone)
    })

  }, [list]);

  const resetItem = () => { setItem(blankItem) }
  const removeItem = (self) => { dispatch( listRemove(self.target.dataset.key) ) }  

  const addItem = (type = null) => {
    setItem(blankItem)
    setModal(true)
    setButton('default')
    setSuggested([])
  };

  const setItemListPrice = (self) => {
    dispatch( listUpdateByIndex({ newData: item, index: self.index }) )
    setModal(false)
    resetItem()
  }

  const setPrice = (self) => {
    let data = {
      productId: list.data[self.target.dataset.key].productId,
      index: self.target.dataset.key,
      name: list.data[self.target.dataset.key].name,
      quantity: list.data[self.target.dataset.key].quantity,
      price: (list.data[self.target.dataset.key].price) ? list.data[self.target.dataset.key].price : 0,
      disablePrice: false,
      disableName: false,
      bought: (!self.target.checked) ? false : true
    }

    setItem(data)
    setModal( (!self.target.checked) ? false : true )

    // save item status if user unchecks the checkbox
    if (!self.target.checked) {
      dispatch( listUpdateByIndex({ newData: data, index: self.target.dataset.key }) )
      resetItem()
    }
    setButton('priceReady')
  }

  const submitItem = () => {
    dispatch( listInsert(item) )
    setModal(false)
    resetItem()
  }

  const submitSuggestedItem = (suggested = null) => {
    if (suggested !== null) {
      dispatch( listInsert({
          productId: suggested._id,
          name: suggested.name,
          price: suggested.price,
          quantity: 1
      })
      )
    }
    setSuggested([])
    setModal(false)
    resetItem()
  }

  const saveList = async () => {
    if(!user.token) {
      setErrorSave({ status: true, message: 'You need to create an account before you can save your list. Registration is FREE  and we will NEVER CHARGE anything from you.'})
      return;
    } else {
      let items = []
      let overAllTotal = 0
      list.data.map( (item) => {
        let i = {
          productId: item.productId,
          name: item.name,
          quantity: item.quantity,
          price : item.price,
          subtotal : ( !isNaN(item.quantity) ) ? parseFloat(item.price)*parseFloat(item.quantity) : item.price
        }
        overAllTotal += i.subtotal
        items.push(i)
      })

      let data = {
        userId: user.id,
        items: items,
        subtotal: overAllTotal,
        purchasedOn: new Date(),
        status: "Active",
        isArchived: false
      }

      dispatch( loadingSet({ status: true, message: 'Saving Grocery List...' }) )

      let api = await API_PRIVATE(data, '/api/order/checkout', 'post')
        .then( (response) => {
          return JSON.parse(response)
        })

      if(api) {
        if( api.hasOwnProperty('errors') ) {
          let errs = ''

          Object.values(api.errors).map( (e) => {
            errs = errs + `${e.message} | `
          })

          setErrorSave({ status: true, message: errs })
        } else {
          dispatch( toastSet({ status: true, message: `Grocery List Saved succesfully!` }) )
          resetItem()
          dispatch( listPurge() )
          setModalDone(false)
          setTotal(0)
        }
        dispatch( loadingSet({ status: false, message: '' }) )
      }
    }
  }

  const [suggested, setSuggested] = useState([]);

  useEffect(() => {
    if(suggested.length > 0) {
      RenderSuggestions()
    }
  }, [suggested]);

  const suggestions = async (keyword) => {
    setItem({ ...item, name: keyword })

    if (keyword.length%3 === 0 && keyword.length >= 3 ) {
      let api = await API_PRIVATE(null, `/api/products/search/${keyword}`, 'get')
        .then( (response) => {
          return JSON.parse(response)
        })

      if(api) {
        setSuggested(api)
      }
    }
  }

  const clearList = () => {
      resetItem()
      dispatch( listPurge() )
      setModalDone(false)
      setTotal(0)
  }

  const shareList = async () => {
      let data = {
        userId: user.id,
        items: list
      }

      dispatch( loadingSet({ status: true, message: 'Sharing Grocery List...' }) )

      let api = await API_PRIVATE(data, '/api/cart/create', 'post')
        .then( (response) => {
          return JSON.parse(response)
        })

      if(api) {
        if( api.hasOwnProperty('errors') ) {
          let errs = ''

          Object.values(api.errors).map( (e) => {
            errs = errs + `${e.message} | `
          })

          setErrorSave({ status: true, message: errs })
        } else {
          dispatch( toastSet({ status: true, message: `Your Share URL is: ${process.env.REACT_APP_DOMAIN}/shared/${api.data._id}` }) )
          resetItem()
          dispatch( listPurge() )
          setModalDone(false)
          setTotal(0)
        }
        dispatch( loadingSet({ status: false, message: '' }) )
      }    
  }

  function RenderSuggestions() {
    const s = [];
    let ctr = 0

    for (const element of suggested) {
      if(ctr >= 3) break;
      s.push(<li key={element._id}>
              <a href="#" onClick={() => submitSuggestedItem(element)}>
                <img src={element.imageUrl} /> <strong>{element.name}</strong>
              </a>
            </li>)
      ctr++
    }

    return(
      <ul className="suggested">{s}</ul>
    )
  }

  function RenderList() {
    return list.data.map((element, index) => { return (
      <div className="list_items" key={index}>
        <Form.Check type="checkbox"  inline>
          <Form.Check.Input isValid data-key={index} onChange={ setPrice } checked={element.bought} />
          <div className={`list_label ${element.bought ? "alreadyBought" : ""}`}>
            <em className="ml-2 mr-2">({element.quantity})</em> &nbsp;
            <strong>{element.name} -- </strong>
            <span>{(element.price > 0) ? element.price : '' }</span>
            <Button data-key={index} onClick={ removeItem } className="float-right">x</Button>
          </div>
        </Form.Check>
      </div>
      );
    })
  }

  return (
  <>
    <Card className="brownCard">
      <Card.Body>
        <Card.Title>
          <h1>
            Grocery List

            <ButtonGroup className="float-right" size="sm" id="grocerySettings">
              <DropdownButton drop="start" as={ButtonGroup} title={<FontAwesomeIcon icon="gear" />} id="bg-nested-dropdown" variant="dark">
                <Dropdown.Item eventKey="1" onClick={shareList}><FontAwesomeIcon icon="share-nodes" /> Share List</Dropdown.Item>
                <Dropdown.Item eventKey="2" onClick={clearList}><FontAwesomeIcon icon="trash" /> Clear List</Dropdown.Item>
              </DropdownButton>          
            </ButtonGroup>
          </h1>
        </Card.Title>
          {
            (list.data.length < 1) ?
            <Alert variant="danger">You have an empty grocery list!</Alert>
            :
            <RenderList />
          }
          <div align="right">
            <Button variant="secondary" onClick={() => addItem('initial')}>
              <FontAwesomeIcon icon="circle-plus" /> Add Item
            </Button>
          </div>

          <div className="totals">
            TOTAL <strong>{total}</strong>
          </div>
      </Card.Body>
    </Card>

    <Modal show={modal} onHide={handleClose} centered>
      <Modal.Body>
        <Form onSubmit={submitItem}>
            <RenderSuggestions />
            <Form.Control
              type="name"
              value={item.name}
              onChange={e => suggestions(e.target.value) }
              autoFocus={button !== 'priceReady'}
              className="mb-3"
              placeholder="Name of Grocery Item"
            />
            <Form.Control
              type="hidden"
              value={item.productId}
              onChange={e => suggestions(e.target.value) }
              className="mb-3"
              placeholder="Product ID"
            />
          <FloatingLabel label="Quantity" className="mb-3" >
            <Form.Control
              type="name"
              value={item.quantity}
              onChange={e => setItem({ ...item, quantity: e.target.value })}
            />
            <Form.Range onChange={e => setItem({ ...item, quantity: e.target.value }) } />
          </FloatingLabel>
          <FloatingLabel label="Price" className="mb-3" >
            <Form.Control
              type="number"
              value={(item.price === 0) ? '' : item.price}
              placeholder={item.price}
              onChange={e => setItem({ ...item, price: e.target.value })}
              autoFocus={button === 'priceReady'}
            />
          </FloatingLabel>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Cancel
        </Button>
        {
          (button === 'priceReady') ?
          <Button variant="primary" onClick={ () => setItemListPrice({ ...item, index: item.index })} >Update</Button>
          :
          <Button variant="primary" onClick={submitItem}>Add this Item</Button>
        }
      </Modal.Footer>
    </Modal>

    <Modal show={modalDone} onHide={handleDoneClose} centered>
      <Modal.Body className="modalMessage">
        YAY! You bought all the items in your list!<br />
        <div className="modalTotals"><strong>Total: {total}</strong></div>
      </Modal.Body>
      <Modal.Footer>
        <Alert variant="danger" show={errorSave.status}>
          <Alert.Heading>OOPSIE!</Alert.Heading>
          {errorSave.message}
        </Alert>
        <Button variant="secondary" onClick={handleDoneClose}>
          OK
        </Button>
        <Button variant="primary" onClick={saveList}>Save and Clear My List</Button>
      </Modal.Footer>
    </Modal>
  </>
  )
}
