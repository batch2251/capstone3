import { Container, Row, Col, Card, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { Navigate } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { AUTH_CHECK } from "@/helpers/auth-check.js"

export default function History(props) {
    const isLoggedIn = AUTH_CHECK()
    return (
  <>
  <Container>
      <Card className="brownCard flexible home">
        <Card.Body>
          <Card.Title><h1 align="center">Sample Template</h1></Card.Title>

          <p>Praesent sed nulla id nunc volutpat vestibulum eu a elit. Suspendisse interdum tempus eros, eget sollicitudin eros facilisis vel. Curabitur gravida leo lacus, eget congue justo lacinia id. Ut eleifend rhoncus tortor, at vestibulum sapien ullamcorper non. Quisque convallis nec risus congue convallis. Proin suscipit dignissim diam. Curabitur non nisl sem. Ut molestie, lorem dapibus bibendum tincidunt, metus orci sodales purus, ac porttitor dui ligula at metus. Nullam turpis ante, auctor nec nibh eget, semper elementum turpis. Vestibulum quis lorem condimentum, facilisis elit non, commodo risus. Vestibulum quis pellentesque libero. Morbi at turpis hendrerit, dictum augue vitae, vestibulum erat. Aenean erat orci, eleifend a ante at, vestibulum imperdiet velit. Praesent finibus in elit sed sagittis. Pellentesque nec erat eu nulla accumsan laoreet placerat sed tellus.</p>
          
          <Button variant="primary" as={Link} to="/grocery-list" size="lg">
            <FontAwesomeIcon color="success" icon="cart-shopping" /> Create A Grocery List
          </Button>

        </Card.Body>
      </Card>
  </Container>
  </>
    )
}
