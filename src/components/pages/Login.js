import { useState, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';

import { useDispatch } from 'react-redux'
import { userSet } from '@/store/slice/userSlice'
import { toastSet } from '@/store/slice/toastSlice'
import { loadingSet } from '@/store/slice/loadingSlice'

import { API_AUTH } from "@/helpers/api-public.js"

import { Card, Form, Button, Alert } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default function Login() {
	const dispatch = useDispatch()
	// State hooks to store the values of the input fields

  const [input, setInput] = useState({
    email: '',
    password: ''
  })

  const [alertMessage, setAlertMessage] = useState({
    status: false,
    message: ''
  })


	// State to determine whether submit button is enable
	const [isActive, setIsActive] = useState(false);
	const navigate = useNavigate();


	useEffect(() => {
		( input.email !== '' && input.password !== '' ) ? setIsActive(true) : setIsActive(false);
	}, [input]);


  const authenticate = async (e) => {
    e.preventDefault()

    dispatch( loadingSet({ status: true, message: 'Logging In' }) )

    let api = await API_AUTH(input, '/api/login')
      .then( (response) => {
        return JSON.parse(response)
      })

    if(api) {
      if( api.hasOwnProperty('error') ) {
        setAlertMessage({ status: true, message: api.error })
      } else {
        localStorage.setItem('listahan_user', JSON.stringify(api))
        dispatch(
          userSet({
            fname: api.firstName,
            lname: api.lastName,
            email: api.email,
            token: api.access,
            id: api.id,
            role: api.isAdmin
          })
        );

        dispatch(
          toastSet({
            status: true,
            message: `Login Successfully as ${api.firstName} ${api.lastName}`
          })
        )

        if(api.role < 1) {
          navigate('/admin/dashboard')
        } else {
          navigate('/my/history')
        }

      }
      dispatch( loadingSet({ status: false, message: '' }) )
    }
  }

    return (
        <>
        <Card className="brownCard">
          <Card.Body>
            <Card.Title><h1>Login</h1></Card.Title>
            <Form onSubmit={ authenticate }>
              <Form.Group controlId="userEmail" className="mb-3">
                  <Form.Label>Email address</Form.Label>
                  <Form.Control
                    type="email"
                    placeholder="Enter email"
                    value={input.email}
                    onChange={e => setInput({ ...input, email: e.target.value })}
                    required
                  />
              </Form.Group>
              <Form.Group controlId="password">
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    type="password"
                    placeholder="Password"
                    value={input.password}
                    onChange={e => setInput({ ...input, password: e.target.value })}
                    required
                  />
              </Form.Group>

              <Alert show={alertMessage.status} variant="danger" className="mt-3 mb-0">
                {alertMessage.message}
              </Alert>

              <hr />
              <div>
                <Button
                  className="mr-4"
                  variant="primary"
                  id="submitBtn"
                  type="submit"
                  onClick={ authenticate }
                  disabled={!isActive}
                  size="lg"
                >
                  <FontAwesomeIcon icon="arrow-right-to-bracket" />  Login {alert.status}
                </Button>

                <Button as={Link} to="/register" variant="text" size="sm" className="float-right">
                  <FontAwesomeIcon icon="pen" />  Create an account
                </Button>
              </div>
            </Form>
          </Card.Body>
        </Card>
        </>
    )

}
