import { useState, useEffect } from 'react';
import { Row, Col, Card, Form, FloatingLabel, Button, Alert, Container} from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';

import { useDispatch } from 'react-redux'
import { loadingSet } from '@/store/slice/loadingSlice'
import { toastSet } from '@/store/slice/toastSlice'

import blankInput from '@/constants/blank.user'
import Countries from '@/constants/countries'
import Roles from '@/constants/user.roles'

import { ADMIN_AUTH_CHECK } from "@/helpers/admin-auth-check.js"
import { API_PRIVATE } from "@/helpers/api-private.js"
import { Navigate, useParams } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default function UserEdit(props) {
    const USER_ID = useParams();

    const isAdmin = ADMIN_AUTH_CHECK()

    const navigate = useNavigate();
    const dispatch = useDispatch()

    const [input, setInput] = useState(blankInput)

    const [activeSwitch, setActiveSwitch] = useState(false)

    useEffect( () => {
          dispatch( loadingSet({ status: true, message: 'Getting User Details...' }) )
          API_PRIVATE(null, `/admin/users/${USER_ID.id}`, 'get')
            .then( (response) => {
              //console.log(response)
              setInput( JSON.parse(response) )
              dispatch( loadingSet({ status: false, message: '' }) )
            })
    }, [USER_ID, dispatch]);

    useEffect( () => {
        if (input.active) {
          setActiveSwitch(true)
        } else {
          setActiveSwitch(false)
        }
    }, [input.active]);

    const [alertMessage, setAlertMessage] = useState({ status: false, message: '' })

    const updateUser = async (e) => {
      e.preventDefault()
      dispatch( loadingSet({ status: true, message: 'Updating user...' }) )

      let api = await API_PRIVATE(input, `/admin/users/${USER_ID.id}`, 'put')
        .then( (response) => {
          return JSON.parse(response)
        })

      if(api) {
        if( api.hasOwnProperty('errors') ) {
          let errs = ''

          if(Object.values.length < 1) return

          Object.values(api.errors).map( (e) => {
            errs = errs + `${e.message} | `
            return e
          })

          setAlertMessage({ status: true, message: errs })
        } else {
          dispatch( toastSet({ status: true, message: `${input.firstName} ${input.lastName} updated succesfully!` }) )
          navigate('/admin/users/list')
        }
        dispatch( loadingSet({ status: false, message: '' }) )
      }
    }

    const RenderCountries = () => {
        return (
          Countries.map( (item, index) => {
            return (
              <option value={item.code} key={index}>{item.name}</option>
            )
          })
        )
    }

    const RenderRoles = () => {
        return (
          Roles.map( (item, index) => {
            return (
              <option value={item.role} key={index}>{item.name}</option>
            )
          })
        )
    }

    return (
      <Container>
        {
          (!isAdmin) ?
            <Navigate to="/not-allowed" />
          :
            <>
            <Form onSubmit={(e) => updateUser(e)}>
            <Row>
              <Col md={9} sm={12}>
                <Card className="adminCard">
                  <Card.Body>
                    <Card.Title>
                      Edit User
                      <Button variant="success" as={Link} to="/admin/users/list">
                        <FontAwesomeIcon color="success" icon="list-check" /> List All Users
                      </Button>
                    </Card.Title>
                    <hr />
                    <Card.Subtitle>All fields marked in RED is REQUIRED.</Card.Subtitle>

                    <Row>
                      <Col md={6} sm={12}>
                        <FloatingLabel label="First Name" className="mb-3" >
                          <Form.Control
                            type="text"
                            isInvalid={!input.firstName}
                            value={input.firstName}
                            onChange={e => setInput({ ...input, firstName: e.target.value })}
                          />
                        </FloatingLabel>
                      </Col>
                      <Col md={6} sm={12}>
                        <FloatingLabel label="Last Name" className="mb-3" >
                          <Form.Control
                            type="text"
                            isInvalid={!input.lastName}
                            value={input.lastName}
                            onChange={e => setInput({ ...input, lastName: e.target.value })}
                          />
                        </FloatingLabel>
                      </Col>
                      <hr />
                      <Col md={6} sm={12}>
                        <FloatingLabel label="Email" className="mb-3" >
                          <Form.Control
                            type="text"
                            isInvalid={!input.email}
                            value={input.email}
                            onChange={e => setInput({ ...input, email: e.target.value })}
                          />
                        </FloatingLabel>
                      </Col>
                      <Col md={6} sm={12}>
                        <FloatingLabel label="Mobile #" className="mb-3" >
                          <Form.Control
                            type="text"
                            isInvalid={!input.mobileNo}
                            value={input.mobileNo}
                            onChange={e => setInput({ ...input, mobileNo: e.target.value })}
                          />
                        </FloatingLabel>
                      </Col>
                      <hr />
                      <Col sm={12}>
                        <FloatingLabel label="Address Line 1" className="mb-3" >
                          <Form.Control
                            type="text"
                            isInvalid={!input.address.street1}
                            value={input.address.street1}
                            onChange={e => setInput({ ...input, address: { ...input.address, street1: e.target.value } })}
                          />
                        </FloatingLabel>
                      </Col>
                      <Col sm={12}>
                        <FloatingLabel label="Address Line 2" className="mb-3" >
                          <Form.Control
                            type="text"
                            value={input.address.street2}
                            onChange={e => setInput({ ...input, address: { ...input.address, street2: e.target.value } })}
                          />
                        </FloatingLabel>
                      </Col>
                      <hr />
                      <Col md={4} sm={12}>
                        <FloatingLabel label="City" className="mb-3" >
                          <Form.Control
                            type="text"
                            isInvalid={!input.address.city}
                            value={input.address.city}
                            onChange={e => setInput({ ...input, address: { ...input.address, city: e.target.value } })}
                          />
                        </FloatingLabel>
                      </Col>
                      <Col md={4} sm={12}>
                        <FloatingLabel label="State/Province" className="mb-3" >
                          <Form.Control
                            type="text"
                            isInvalid={!input.address.province}
                            value={input.address.province}
                            onChange={e => setInput({ ...input, address: { ...input.address, province: e.target.value } })}
                          />
                        </FloatingLabel>
                      </Col>
                      <Col md={4} sm={12}>
                        <FloatingLabel label="Postal Code" className="mb-3" >
                          <Form.Control
                            type="text"
                            isInvalid={!input.address.postalCode}
                            value={input.address.postalCode}
                            onChange={e => setInput({ ...input, address: { ...input.address, postalCode: e.target.value } })}
                          />
                        </FloatingLabel>
                      </Col>
                      <hr />
                      <Col sm={12}>
                        <FloatingLabel label="Country" className="mb-3" >
                          <Form.Select
                            isInvalid={!input.address.country}
                            value={input.address.country}
                            onChange={e => setInput({ ...input, address: { ...input.address, country: e.target.value } })}
                          >
                            <option>Select Your Country</option>
                            <RenderCountries />
                          </Form.Select>
                        </FloatingLabel>
                      </Col>
                    </Row>
                  </Card.Body>
                </Card>
              </Col>

              <Col md={3} sm={12}>
                <Card className="adminCard">
                  <Card.Body>
                    <div align="right" className="mb-2 switch">
                      <Form.Check
                        type="switch"
                        id="custom-switch"
                        label={ (activeSwitch) ? 'Active User' : 'Inactive User'}
                        inline
                        isInvalid={!activeSwitch}
                        isValid={activeSwitch}
                        checked={activeSwitch}
                        onChange={e => setInput({ ...input, active: e.target.checked })}
                      />
                    </div>
                    <hr />
                    <FloatingLabel label="User Level" className="mb-3" >
                      <Form.Select
                        name="Role"
                        isInvalid={!input.role}
                        value={input.role}
                        onChange={e => setInput({ ...input, role: e.target.value })}
                      >
                        <option>Select User Level:</option>
                        <RenderRoles />
                      </Form.Select>
                    </FloatingLabel>
                    <Alert show={alertMessage.status} variant="danger" className="mt-3 mb-0">
                      {alertMessage.message}
                    </Alert>
                    <hr />
                    <div align="right">
                      <Button
                        className="mt-3"
                        variant="primary"
                        type="submit"
                        id="submitBtn"
                        onSubmit={updateUser}
                        size="lg"
                      >
                        <FontAwesomeIcon color="success" icon="floppy-disk" /> Update User
                      </Button>
                    </div>
                  </Card.Body>
                </Card>
              </Col>

            </Row>
            </Form>
            </>
        }
      </Container>
    )
}
