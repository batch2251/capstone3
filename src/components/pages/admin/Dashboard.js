import React, { useState, useEffect } from 'react';
import { Link, Navigate, useNavigate } from 'react-router-dom'

import { Container, Row, Col, Card, Button, Form } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import AdminLineChart from '@/components/charts/AdminLineChart';
import AdminDoughnutChart from '@/components/charts/AdminDoughnutChart';

import { useDispatch } from 'react-redux'
import { filterSet } from '@/store/slice/filterSlice'

import Months from '@/constants/constant.months'
import { ADMIN_AUTH_CHECK } from "@/helpers/admin-auth-check.js"

export default function Dashboard(props) {
    const isAdmin = ADMIN_AUTH_CHECK()

    const [input, setInput] = useState({month: '', year: ''})
    const [submit, setSubmit] = useState(true)
    const navigate = useNavigate();
    const dispatch = useDispatch()

    useEffect(() => {
        if(input.month && input.year) {
          setSubmit(false)
        } else {
          setSubmit(true)
        }
    }, [input]);    

    const RenderMonths = () => {
        return (
          Months.map( (item, index) => {
            return (
              <option value={item} key={index}>{item}</option>
            )
          })
        )
    }   

    const RenderYear = () =>  {
        let date = new Date()
        let year = date.getFullYear()      
        let items = [];
        for (let page = year; page >= (year - 5); page--) {
          items.push(
            <option key={page} value={page}>{page}</option>,
          );
        }                  
        return (
          <>{items}</>
        )      
    }   

    const updateDate = (e) => {
      e.preventDefault()
      // dispatch( loadingSet({ status: true, message: 'Updating Profile...' }) )
      dispatch( filterSet({ month: input.month, year: input.year }) )      
      navigate(`/admin/dashboard?month=${input.month}&year${input.year}`)
    }

    return (
  <>
    {(!isAdmin) ?
      <Navigate to="/not-allowed" />
    :
      <Container>
          <Card className="brownCard flexible home">
            <Card.Body>
              <Card.Title>
                <h1 align="center">Admin Dashboard</h1>
                <Form onSubmit={(e) => updateDate(e)} id="dataFilter">
                  <Row>
                    <Col md={5} sm={12}>
                      <Form.Select value={input.month} onChange={e => setInput({ ...input, month: e.target.value })}>
                        <option>Select Month</option>
                        <RenderMonths />
                      </Form.Select>              
                    </Col>

                    <Col md={5} sm={12}>
                      <Form.Select value={input.year} onChange={e => setInput({ ...input, year: e.target.value })}>
                        <option>Select Year</option>
                        <RenderYear />
                      </Form.Select>               
                    </Col>   

                    <Col md={2} sm={12}>           
                      <Button type="submit" disabled={submit}>Apply</Button>
                    </Col>
                  </Row> 
                </Form>
              </Card.Title>
              
              <hr />
              <Row className="mt-5">
                <Col md={8} sm={12}>
                  <AdminLineChart />
                </Col>

                <Col md={4} sm={12}>
                  <AdminDoughnutChart />
                </Col>                
              </Row>

              <p>Praesent sed nulla id nunc volutpat vestibulum eu a elit. Suspendisse interdum tempus eros, eget sollicitudin eros facilisis vel. Curabitur gravida leo lacus, eget congue justo lacinia id. Ut eleifend rhoncus tortor, at vestibulum sapien ullamcorper non. Quisque convallis nec risus congue convallis. Proin suscipit dignissim diam. Curabitur non nisl sem. Ut molestie, lorem dapibus bibendum tincidunt, metus orci sodales purus, ac porttitor dui ligula at metus. Nullam turpis ante, auctor nec nibh eget, semper elementum turpis. Vestibulum quis lorem condimentum, facilisis elit non, commodo risus. Vestibulum quis pellentesque libero. Morbi at turpis hendrerit, dictum augue vitae, vestibulum erat. Aenean erat orci, eleifend a ante at, vestibulum imperdiet velit. Praesent finibus in elit sed sagittis. Pellentesque nec erat eu nulla accumsan laoreet placerat sed tellus.</p>
              <div align="center">
              <Button variant="primary" as={Link} to="/grocery-list" size="lg">
                <FontAwesomeIcon color="success" icon="cart-shopping" /> Create A Grocery List
              </Button>
              </div>
            </Card.Body>
          </Card>
      </Container>
    }
  </>
    )
}
