import { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux'
import { useParams } from 'react-router-dom';

import { Container, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import { API_PRIVATE } from "@/helpers/api-private.js"

import { loadingSet } from '@/store/slice/loadingSlice'

export default function OrderDetails(props) {
    const USER_ID = useParams();

    const dispatch = useDispatch()    

    const [data, setData] = useState({
    	purchasedOn: '',
    	items: [],
    	subtotal: ''
    })

    const dateOptions = { 
    	weekday: 'long', 
    	year: 'numeric', 
    	month: 'long',
    	day: 'numeric', 
    	hour: '2-digit',
    	minute: '2-digit'
	}

    useEffect(() => {
        dispatch( loadingSet({ status: true, message: 'Fetching Profile...' }) )
        API_PRIVATE(null, `/api/users/orders/${USER_ID.id}`, 'get')
          .then( (response) => {
            setData( JSON.parse(response)[0] )
            dispatch( loadingSet({ status: false, message: '' }) )
        })
    }, [USER_ID, dispatch]);    

    function RenderItems() {
    	return(	
	    	data.items.map( (i, key) => {
	    		return (
	    			<li key={key}>
	    				({i.quantity}) 
	    				{ (i.productId !== null) ?  <Link to={`/admin/product/${i.productId}`}>{i.name}</Link> : i.name } x  <strong>{i.price}</strong>
	    				<em>{i.subtotal}</em>
	    			</li>
	    		)
	    	})
	    )	
    }

    return (
  <>
  <Container>
      <Card className="brownCard flexible">
        <Card.Body>
          <Card.Title><h1 align="center">Expense Details: <span>{ new Date(data.purchasedOn).toLocaleDateString("en-US", dateOptions) }</span></h1></Card.Title>
          
          <div className="receipt">          	
          	<h4 align="center">Items bought</h4>
          	<ul>
          		<RenderItems />
          	</ul>
          	<h5><span>Total: {data.subtotal}</span></h5>
          </div>
        </Card.Body>
      </Card>
  </Container>
  </>
    )
}
