import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Card, Table, Pagination } from 'react-bootstrap';
import { Link, Navigate, useParams } from 'react-router-dom';

import { ADMIN_AUTH_CHECK } from "@/helpers/admin-auth-check.js"
import { API_PRIVATE } from "@/helpers/api-private.js"

import { useDispatch } from 'react-redux'
import { loadingSet } from '@/store/slice/loadingSlice'

export default function OrderList(props) {
    const ORDER_DATE = useParams();
    const isAdmin = ADMIN_AUTH_CHECK()
    const dispatch = useDispatch()

    const [data, setData] = useState([{
    	data: []
    }])

    const apiCall = (page) => {
      let params_page = (page) ? `?page=${page}` : `?page=1`
      //navigate(`/admin/products/list${params_page}`)
      dispatch( loadingSet({ status: true, message: 'Fetching Data...' }) )
      API_PRIVATE(null, `/api/order/list/daily/${ORDER_DATE.date}${params_page}`, 'get')
        .then( (response) => {
          let d = JSON.parse(response)
          // console.log(d) 
          setData( d )
          dispatch( loadingSet({ status: false, message: '' }) )
        })      
    }

    useEffect( () => {
      apiCall()
    }, [ORDER_DATE]);

    const paginate = (e, page) => {
      e.preventDefault()      
      apiCall(page)
    }

    const RenderData = () =>  {      
 		//console.log(data[0].data)
        if(!data[0].data) return
        return (
          data[0].data.map( (item, index) => {
            return (
              <tr key={index}>
                <td align="center">                	
                  <Link to={`/admin/order/${item.id}`}>
                    View Grocery List
                  </Link>                   	
                </td>
                <td vertical-align="center">
                  <Link to={`/admin/user/${item.user.id}`}>
                    {item.user.fname} {item.user.lname}                 
                  </Link>                   
                </td>
                <td>{item.subtotal}</td>
                <td>{ new Date(item.date).toLocaleDateString("en-US")}</td>
              </tr>
            )
          })
        )
    }

    const RenderPagination = () =>  {
        if(!data.result) return
          let items = [];
          for (let page = 1; page <= data.total_pages; page++) {
            items.push(
              <Pagination.Item key={page} active={page === data.current_page} onClick={e => paginate(e, page)} href={`/admin/users/list?page=${page}`}>
                {page}
              </Pagination.Item>,
            );
          }                  
          return (
            <>
              <Pagination>{items}</Pagination>
            </>
          )      
    }

    return (
      <Container>
        <Row>
          <Col>
                {
                  (!isAdmin) ?
                    <Navigate to="/not-allowed" />
                  :
                    <>
                      <Card className="adminCard">
                        <Card.Body>
                          <Card.Title>
                            App Grocery List
                          </Card.Title>
                          <hr />
                          {/*<Card.Subtitle className="mb-2 text-muted">Card Subtitle</Card.Subtitle>*/}

                          <Table striped bordered hover className="adminTable" responsive>
                            <thead>
                              <tr>
                                <th>Details</th>
                                <th>User</th>
                                <th>Price</th>
                                <th>Created On</th>
                              </tr>
                            </thead>
                            <tbody>
                              <RenderData />
                            </tbody>
                          </Table>
                          <RenderPagination />
                        </Card.Body>
                      </Card>
                    </>
                }
            </Col>
        </Row>
      </Container>
    )
}
