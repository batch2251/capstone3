import { useState, useEffect } from 'react';
import { Row, Col, Card, Form, FloatingLabel, Button, Alert, Container} from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';

import { useDispatch } from 'react-redux'
import { loadingSet } from '@/store/slice/loadingSlice'
import { toastSet } from '@/store/slice/toastSlice'

import blankInput from '@/constants/blank.product'

import { ADMIN_AUTH_CHECK } from "@/helpers/admin-auth-check.js"
import { API_PRIVATE } from "@/helpers/api-private.js"
import { Navigate, useParams } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default function ProductEdit(props) {
    const PRODUCT_ID = useParams();

    const isAdmin = ADMIN_AUTH_CHECK()

    const navigate = useNavigate();
    const dispatch = useDispatch()

    const [input, setInput] = useState(blankInput)
    const [activeSwitch, setActiveSwitch] = useState(false)
    const [featuredSwitch, setFeaturedSwitch] = useState(false)

    useEffect( () => {
          dispatch( loadingSet({ status: true, message: 'Getting Product Details...' }) )
          API_PRIVATE(null, `/admin/product/${PRODUCT_ID.id}`, 'get')
            .then( (response) => {
              setInput( JSON.parse(response) )
              dispatch( loadingSet({ status: false, message: '' }) )
            })
    }, [PRODUCT_ID, dispatch]);

    useEffect( () => {
        if (input.isActive) {
          setActiveSwitch(true)
        } else {
          setActiveSwitch(false)
        }
    }, [input.isActive]);

    useEffect( () => {
        if (input.featured) {
          setFeaturedSwitch(true)
        } else {
          setFeaturedSwitch(false)
        }
    }, [input.featured]);

    const [alertMessage, setAlertMessage] = useState({ status: false, message: '' })

    const updateProduct = async (e) => {
      e.preventDefault()
      dispatch( loadingSet({ status: true, message: 'Updating product...' }) )

      let api = await API_PRIVATE(input, `/admin/product/${PRODUCT_ID.id}`, 'put')
        .then( (response) => {
          return JSON.parse(response)
        })

      if(api) {
        if( api.hasOwnProperty('errors') ) {
          let errs = ''

          Object.values(api.errors).map( (e) => {
            errs = errs + `${e.message} | `
            return e
          })

          setAlertMessage({ status: true, message: errs })
        } else {
          dispatch( toastSet({ status: true, message: `${input.name} updated succesfully!` }) )
          navigate('/admin/products/list')
        }
        dispatch( loadingSet({ status: false, message: '' }) )
      }
    }

    return (
      <Container>
                {
                  (!isAdmin) ?
                    <Navigate to="/not-allowed" />
                  :
                    <>
                      <Form onSubmit={(e) => updateProduct(e)}>
                      <Row>
                        <Col md={9} sm={12}>                    
                          <Card className="adminCard">
                            <Card.Body>
                              <Card.Title>
                                Edit Product
                                <Button variant="success" as={Link} to="/admin/products/list">
                                  <FontAwesomeIcon color="success" icon="list-check" /> List All Products
                                </Button>
                              </Card.Title>
                              <hr />
                              <Card.Subtitle>All fields marked in RED is REQUIRED.</Card.Subtitle>
                                <Row>
                                  <Col sm={12}>
                                    <FloatingLabel label="Product Name" className="mb-3" >
                                      <Form.Control
                                        type="text"
                                        isInvalid={!input.name}
                                        value={input.name}
                                        onChange={e => setInput({ ...input, name: e.target.value })}
                                      />
                                    </FloatingLabel>
                                  </Col>
                                  <hr />
                                  <Col md={6} sm={12}>
                                    <FloatingLabel label="Product Description" className="mb-3" >
                                      <Form.Control
                                        as="textarea"
                                        style={{ height: '150px' }}
                                        isInvalid={!input.description}
                                        value={input.description}
                                        onChange={e => setInput({ ...input, description: e.target.value })}
                                      />
                                    </FloatingLabel>
                                  </Col>
                                  <Col md={6} sm={12}>
                                    <FloatingLabel label="Product Keywords" className="mb-3" >
                                      <Form.Control
                                        as="textarea"
                                        style={{ height: '150px' }}
                                        isInvalid={!input.keywords}
                                        value={input.keywords}
                                        onChange={e => setInput({ ...input, keywords: e.target.value })}
                                      />
                                      <Form.Text muted>
                                        Keywords must be separated by comma. i.e.(cap, hat, headgear)
                                      </Form.Text>
                                    </FloatingLabel>
                                  </Col>
                                  <hr />
                                  <Col md={4} sm={12}>
                                    <FloatingLabel label="Price" className="mb-3" >
                                      <Form.Control
                                        type="number"
                                        isInvalid={!input.price}
                                        value={input.price}
                                        onChange={e => setInput({ ...input, price: e.target.value })}
                                      />
                                    </FloatingLabel>
                                  </Col>
                                  <Col md={8} sm={12}>
                                    <FloatingLabel label="Product Image URL" className="mb-3" >
                                      <Form.Control
                                        type="text"
                                        isInvalid={!input.imageUrl}
                                        value={input.imageUrl}
                                        onChange={e => setInput({ ...input, imageUrl: e.target.value })}
                                      />
                                    </FloatingLabel>
                                  </Col>
                                </Row>
                            </Card.Body>
                          </Card>
                        </Col>

                        <Col md={3} sm={12}>
                          <Card className="adminCard">
                            <Card.Body>
                              <div align="right" className="mb-2 switch">
                                <Form.Check
                                  type="switch"
                                  label={ (activeSwitch) ? 'Active Product' : 'Inactive Product'}
                                  inline
                                  isInvalid={!activeSwitch}
                                  isValid={activeSwitch}
                                  checked={activeSwitch}
                                  onChange={e => setInput({ ...input, isActive: e.target.checked })}
                                />
                              </div>
                              <hr />
                              <div align="right" className="mb-2 switch">
                                <Form.Check
                                  type="switch"
                                  label={ (featuredSwitch) ? 'Featured Product' : 'Not Featured Product'}
                                  inline
                                  isInvalid={!featuredSwitch}
                                  isValid={featuredSwitch}
                                  checked={featuredSwitch}
                                  onChange={e => setInput({ ...input, featured: e.target.checked })}
                                />
                              </div>                            
                              <Alert show={alertMessage.status} variant="danger" className="mt-3 mb-0">
                                {alertMessage.message}
                              </Alert>
                              <hr />
                              <div align="right">
                                <Button
                                  className="mt-3"
                                  variant="primary"
                                  type="submit"
                                  id="submitBtn"
                                  onSubmit={updateProduct}
                                  size="lg"
                                >
                                  <FontAwesomeIcon color="success" icon="floppy-disk" /> Update Product
                                </Button>
                              </div>                            
                            </Card.Body>
                          </Card>  
                        </Col>
                      </Row>     
                      </Form>

                    </>
                }

      </Container>
    )
}
