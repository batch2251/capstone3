import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Card, Table, Button, Pagination, Form, InputGroup } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import { ADMIN_AUTH_CHECK } from "@/helpers/admin-auth-check.js"
import { API_PRIVATE } from "@/helpers/api-private.js"

import { useDispatch } from 'react-redux'
import { loadingSet } from '@/store/slice/loadingSlice'

import { Navigate, useNavigate } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default function ProductsList(props) {
    const isAdmin = ADMIN_AUTH_CHECK()
    const navigate = useNavigate()

    const dispatch = useDispatch()
    const [searchInput, setSearchInput] = useState('')

    const [data, setData] = useState({})
    useEffect( () => {
      apiCall()
    }, []);

    useEffect( () => {
      RenderProducts()
    }, [data]);    

    useEffect( () => {
      if (searchInput.length === 0) {
        apiCall()
      }
    }, [searchInput]);              

    const paginate = (e, page) => {
      e.preventDefault()
      apiCall(page)
    }

    const apiCall = (page) => {
      let params_page = (page) ? `?page=${page}` : `?page=1`
      navigate(`/admin/products/list${params_page}`)
      dispatch( loadingSet({ status: true, message: 'Getting products...' }) )
      API_PRIVATE(null, `/admin/product/list${params_page}`, 'get')
        .then( (response) => {
          setData( JSON.parse(response) )
          dispatch( loadingSet({ status: false, message: '' }) )
        })
    }

    const RenderProducts = () =>  {
        if(!data.result) return
        return (
          data.result.map( (item, index) => {
            return (
              <tr key={index}>
                <td align="center">
                  {
                    (item.isActive) ?
                      <FontAwesomeIcon className="faSuccess" icon="circle-check" size="lg" />
                    :
                      <FontAwesomeIcon className="faError" icon="circle-xmark" size="lg" />
                  }
                </td>
                <td><img src={item.imageUrl} alt={item.name} height="50" /></td>
                <td vertical-align="center">
                  <Link to={`/admin/product/${item._id}`}>
                    {item.name}
                  </Link>
                  { (item.featured) ?
                    <sup><FontAwesomeIcon className="faGold" icon="star" size="lg" /> Featured</sup>
                    :
                    <span></span>
                  }
                </td>
                <td>{item.price}</td>
                <td>{ new Date(item.createdOn).toLocaleDateString("en-US")}</td>
              </tr>
            )
          })
        )
    }

    const RenderPagination = () =>  {
        if(!data.result) return
          let items = [];
          for (let page = 1; page <= data.total_pages; page++) {
            items.push(
              <Pagination.Item key={page} active={page === data.current_page} onClick={e => paginate(e, page)} href={`/admin/users/list?page=${page}`}>
                {page}
              </Pagination.Item>,
            );
          }
          return (
            <>
              <Pagination>{items}</Pagination>
            </>
          )
    }

    const searchSubmit = async (e) => {
      e.preventDefault()

      dispatch( loadingSet({ status: true, message: `Searching for ${searchInput}...` }) )
      let api = await API_PRIVATE(null, `/admin/product/admin-search?keyword=${searchInput}`, 'post')
        .then( (response) => {
          return JSON.parse(response)
        })

      if(api) {
        if( api.hasOwnProperty('error') ) {
          let errors = ''
          Object.entries(api.error.errors).map( (e) => {
            errors = errors + e[1].message + '. '
            return e
          })
        } else {
          setData(api)
        }
        dispatch( loadingSet({ status: false, message: '' }) )
      }

    }

    return (
      <Container>
        <Row>
          <Col>
                {
                  (!isAdmin) ?
                    <Navigate to="/not-allowed" />
                  :
                    <>
                      <Card className="adminCard">
                        <Card.Body>
                          <Card.Title>
                            Products List
                            <Button variant="success" as={Link} to="/admin/products/add"><FontAwesomeIcon color="success" icon="circle-plus" />  Add New Product</Button>
                          </Card.Title>
                          <hr />
                          <Card.Subtitle className="mb-2">
                            <Form id="searchForm" onSubmit={(e) => searchSubmit(e)}>
                              <InputGroup className="mb-3">
                                <InputGroup.Text>
                                  <Button variant="text" onClick={e => setSearchInput('')} hidden={searchInput.length === 0}>Reset Search</Button>
                                </InputGroup.Text>                              
                                <Form.Control
                                  placeholder="Search for product using 'Product name' as keyword"
                                  value={searchInput}
                                  onChange={e => setSearchInput(e.target.value)}
                                />
                                <InputGroup.Text>
                                  <Button type="submit" disabled={searchInput.length < 3}>
                                    <FontAwesomeIcon icon="search" /> Search
                                  </Button>
                                </InputGroup.Text>
                              </InputGroup>
                            </Form>
                          </Card.Subtitle>

                          <Table striped bordered hover className="adminTable">
                            <thead>
                              <tr>
                                <th>Active?</th>
                                <th>Image</th>
                                <th>Product Name</th>
                                <th>Price</th>
                                <th>Created On</th>
                              </tr>
                            </thead>
                            <tbody>
                              <RenderProducts />
                            </tbody>
                          </Table>
                          <RenderPagination />
                        </Card.Body>
                      </Card>
                    </>
                }
            </Col>
        </Row>
      </Container>
    )
}
