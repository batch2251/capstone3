import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React, { useState, useEffect } from 'react';
import { useNavigate, Link } from 'react-router-dom'

import { useDispatch } from 'react-redux'
import { loadingSet } from '@/store/slice/loadingSlice'

import { Container, Row, Col, Card, Pagination} from 'react-bootstrap';

import FeaturedCarousel from '@/components/sections/FeaturedCarousel';

import { API_PUBLIC } from "@/helpers/api-public.js"

export default function Suggested(props) {
    const dispatch = useDispatch()
    const navigate = useNavigate()

    const [data, setData] = useState({ result: [] })

    useEffect( () => {
      apiCall()
    }, []);

    const paginate = (e, page) => {
      e.preventDefault()      
      apiCall(page)
    }    

    const apiCall = (page) => {
      let params_page = (page) ? `?page=${page}` : `?page=1`
      navigate(`/suggested${params_page}`)

      dispatch( loadingSet({ status: true, message: 'Fetching data...' }) )
      API_PUBLIC(null, `/api/products${params_page}`, 'get')
        .then( (response) => {
          setData( JSON.parse(response) )
          dispatch( loadingSet({ status: false, message: '' }) )
        })      
    }

    function RenderPagination () {
        if(data.result.length < 1) return
          let items = [];
          for (let page = 1; page <= data.total_pages; page++) {
            items.push(
              <Pagination.Item key={page} active={page === data.current_page} onClick={e => paginate(e, page)} href={`/admin/users/list?page=${page}`}>
                {page}
              </Pagination.Item>,
            );
          }    
          
          return (
            <>
              <Pagination>{items}</Pagination>
            </>
          )      
    }

    function RenderProducts () {
    	if( data.result.length < 1) return

        return (
          data.result.map( (item, index) => {
            return (
              <Col key={index} md={4} sm={12}>
			    <Card className="productsFront">
            <Link to={`/details/${item._id}`}>          
			      <Card.Img variant="top" src={item.imageUrl} />
			      <Card.Body>
			        <Card.Title>
	                    {item.name}                 
	                  { (item.featured) ?
	                    <sup><FontAwesomeIcon className="faGold" icon="star" size="lg" /> Featured</sup>
	                    :
	                    <span></span>
	                  }   
			        </Card.Title>
			        <Card.Text>SRP: {item.price}</Card.Text>
			      </Card.Body>
            </Link>                                 
			    </Card>        
              </Col>
            )
          })
        )    		
    }

    return (
  <>
  <Container>
  	  <FeaturedCarousel	/>
      <Card className="brownCard flexible home">
        <Card.Body>
          <Card.Title><h1 align="center">Suggested Items For Your Grocery List</h1></Card.Title>

		  <RenderPagination />

          <Row>
          	<RenderProducts />
          </Row>

          <RenderPagination />
        </Card.Body>
      </Card>
  </Container>
  </>
    )
}
