import React, { useEffect } from 'react';
import { Navigate } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { userPurge } from '@/store/slice/userSlice'


export default function Logout() {
	document.body.classList.remove('admin')
	const dispatch = useDispatch()

  	useEffect(() => {
		  dispatch( userPurge() )
  	});

	return (
		<Navigate to="/login"/>
	)
}
