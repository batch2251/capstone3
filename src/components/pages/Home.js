import { useEffect } from 'react'
import { Card, Button, Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Img from '@/images/home-bg.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { useSelector, useDispatch } from 'react-redux'
import { userGet } from '@/store/slice/userSlice'

export default function Home() {
  const user = useSelector((state) => state.user)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch( userGet() )
  }, [user, dispatch]);

	return (
	<>
  <Container>
      <Card className="brownCard flexible home">
        <Card.Body>
          <Card.Title><h1 align="center">Mabuhay</h1></Card.Title>
          <Row>
            <Col md={4} sm={12}>
              <img src={Img} className="mb-4" alt="girl throwing lists in thrashcan" />

              { 
                (!user.token) ?
                <Row>
                  <Col md={6} sm={12}>
                    <Button variant="warning" as={Link} to="/register" size="lg" className="mb-3 d-block">
                      <FontAwesomeIcon color="success" icon="pen" /> &nbsp; Register
                    </Button>
                  </Col>
                  <Col md={6} sm={12}>
                    <Button variant="danger" as={Link} to="/login" size="lg" className="mb-3 d-block">
                      <FontAwesomeIcon icon="arrow-right-to-bracket" /> &nbsp; Login
                    </Button>
                  </Col>                
                </Row> 
                :
                ''
              }
            </Col>

            <Col md={8} sm={12}>
              <h5>Say goodbye to those pens and papers that give you ouchy fingers!</h5>
              <p>Say hello to stress-free shopping lists with our Listahan.app, We got you covered.., from Apples to Zucchini and everything in between. With its user-friendly design, Listahan.app makes shopping easier than ever. It has advanced search and sorting capabilities, so you can quickly find the items you need. Whether you're shopping for food, household items, or pet supplies.</p>
              <p><strong>You'll have more time to spend on the important things in life, like deciding between regular and organic fruits and vegetables.</strong></p>

              <hr/>

              <p align="center" className="mt-3">
                <Button variant="primary" as={Link} to="/grocery-list" size="lg" className="mb-3 d-block">
                  <FontAwesomeIcon color="success" icon="cart-shopping" /> Create A Grocery List
                  </Button>
              </p>
            </Col>
          </Row>
        </Card.Body>
      </Card>
  </Container>
	</>

	)
}
