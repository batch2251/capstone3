import { Container, Card, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default function NotLogged() {
    return (
        <>
          <Container>
            <Card className="brownCard">
              <Card.Body>
                <Card.Title><h1 align="center">It's not us, It's YOU!</h1></Card.Title>
                <p align="center">You need to be logged in to access this feature.</p>
                <hr />
                <div align="center">
                  <Button as={Link} to="/register" size="lg">
                    <FontAwesomeIcon color="success" icon="pen" />  Create an account!
                  </Button>
                </div>
                <hr />
                <p className="mb-0" align="center">
                  It's FREE <em>(it's a promise)</em>.
                </p>
              </Card.Body>
            </Card>
          </Container>
        </>
    )
}
