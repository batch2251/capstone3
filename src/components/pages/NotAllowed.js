import { Container, Card} from 'react-bootstrap';
//import { Link } from 'react-router-dom';

export default function NotAllowed() {
    return (
        <>
          <Container>
            <Card className="brownCard">
              <Card.Body>
                <Card.Title><h1 align="center">Stop.... Hold on.....</h1></Card.Title>
                <p align="center">
                  You can't be here. <br />
                  No one else can be here.<br />
                  Unless you steal my heart and break down my walls.<br />
                  You can't be here....
                </p>
              </Card.Body>
            </Card>
          </Container>
        </>
    )
}
