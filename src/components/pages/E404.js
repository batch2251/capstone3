import { Container, Card} from 'react-bootstrap';
//import { Link } from 'react-router-dom';

export default function E404() {
    const message = [
        'Not until we are lost do we begin to understand ourselves.',
        'In the middle of the journey of our life I came to myself within a dark wood where the straight way was lost.',
        'When you\'re feeling lost, take heart. It\'s just your brain gathering the information it needs to make good decisions.',
        'Feeling lost? Take a dream and convert it into small goals, then start taking the steps to hit those goals.',
        'It is an ironic habit of human beings to run faster when we have lost our way.',
        'I need to have a reason why I\'m doing something. Otherwise I\'m lost.',
        'When all else is lost, the future still remains.',
        'You got to go down a lot of wrong roads to find the right one.',
        'When I feel lost and can\'t make a decision, I just stop and get quiet. I take a time-out.',
        'The art of being bored is lost.',
        'Feeling lost? Good! Now you get to walk new paths that lead to much better places.',
        'I lost my head for a little while.',
        'Be Thankful for all things in life. Even for the bad things. SOMETIMES the worst situations in life turn out to be the best things that ever happened to us.',
        'The best thing we can do is to make wherever we\'re lost in look as much like home as we can.',
        'To be lost is as legitimate a part of your process as being found.',
        'Don\'t dwell on what went wrong. Instead, focus on what to do next. Spend your energies on moving forward toward finding the answer.'
    ]

    return (
        <>
          <Container>
            <Card className="brownCard">
              <Card.Body>
                <Card.Title><h1 align="center">ERROR 404 <br />Looking for something?</h1></Card.Title>
                <p align="center">
                  { message[Math.floor(Math.random() * message.length)] }
                </p>
              </Card.Body>
            </Card>
          </Container>
        </>
    )
}
