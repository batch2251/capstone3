import React, { useEffect, useState } from 'react';
import { Container, Row, Col, Card, Button, Form, FloatingLabel, Alert} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { Navigate } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { useDispatch } from 'react-redux'
// import { listInsert, listUpdateByIndex, listRemove, listPurge } from '@/store/slice/listSlice'
// import { userGet } from '@/store/slice/userSlice'
import { loadingSet } from '@/store/slice/loadingSlice'
import { toastSet } from '@/store/slice/toastSlice'

import { AUTH_CHECK } from "@/helpers/auth-check.js"
import { API_PRIVATE } from "@/helpers/api-private.js"

import blankInput from '@/constants/blank.user'
import Countries from '@/constants/countries'

export default function Profile(props) {
  const [input, setInput] = useState(blankInput)
  const [alertMessage, setAlertMessage] = useState({ status: false, message: '' })
  const [isActive, setIsActive] = useState(false);

  const isLoggedIn = AUTH_CHECK()
  // const user = useSelector((state) => state.user)
  const dispatch = useDispatch()


  useEffect(() => {
      dispatch( loadingSet({ status: true, message: 'Fetching Profile...' }) )

      API_PRIVATE(null, '/api/users/profile', 'get')
        .then( (response) => {
          setInput(JSON.parse(response))
          dispatch( loadingSet({ status: false, message: '' }) )
      })
  }, [dispatch]);

	useEffect(() => {
		if(
      input.firstName !== "" &&
      input.lastName !== "" &&
      input.email !== "" &&
      input.password !== "" &&
      input.address.street1 !== "" &&
      input.address.city !== "" &&
      input.address.province !== "" &&
      input.address.postalCode !== ""
    )
		{
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [input]);  

    const updateProfile = async (e) => {
      e.preventDefault()
      dispatch( loadingSet({ status: true, message: 'Updating Profile...' }) )

      let api = await API_PRIVATE(input, `/api/users/profile`, 'put')
        .then( (response) => {
          return JSON.parse(response)
        })

      if(api) {
        if( api.hasOwnProperty('errors') ) {
          let errs = ''

          if (Object.values.length < 1) return

          Object.values(api.errors).map( (e) => {
            errs = errs + `${e.message} | `
            return e
          })

          setAlertMessage({ status: true, message: errs })
        } else {
          dispatch( toastSet({ status: true, message: `Profile updated succesfully!` }) )
          //navigate('/admin/users/list')
        }
        dispatch( loadingSet({ status: false, message: '' }) )
      }
    }

  const RenderCountries = () => {
      return (
        Countries.map( (item, index) => {
          return (
            <option value={item.code} key={index}>{item.name}</option>
          )
        })
      )
  }


    return (
  	<>
      <Container>
      { !isLoggedIn ? 
        <Navigate to="/not-logged-in" />
        :
        <Form onSubmit={(e) => updateProfile(e)}>
        <Row>
          <Col md={9} sm={12}>                    
            <Card className="adminCard">
              <Card.Body>
                <Card.Title>
                  Edit Profile
                </Card.Title>
                <hr />
                <Card.Subtitle>All fields marked in RED is REQUIRED.</Card.Subtitle>
                  <Row>

                    <Col md={6} sm={12}>
                      <FloatingLabel label="First Name" className="mb-3" >
                        <Form.Control
                          type="text"
                          isInvalid={!input.firstName}
                          value={input.firstName}
                          onChange={e => setInput({ ...input, firstName: e.target.value })}
                        />
                      </FloatingLabel>
                    </Col>
                    <Col md={6} sm={12}>
                      <FloatingLabel label="Last Name" className="mb-3" >
                        <Form.Control
                          type="text"
                          isInvalid={!input.lastName}
                          value={input.lastName}
                          onChange={e => setInput({ ...input, lastName: e.target.value })}
                        />
                      </FloatingLabel>
                    </Col>
                    <hr />
                    <Col md={6} sm={12}>
                      <FloatingLabel label="Email" className="mb-3" >
                        <Form.Control
                          type="text"
                          isInvalid={!input.email}
                          value={input.email}
                          onChange={e => setInput({ ...input, email: e.target.value })}
                        />
                      </FloatingLabel>
                    </Col>
                    <Col md={6} sm={12}>
                      <FloatingLabel label="Mobile #" className="mb-3" >
                        <Form.Control
                          type="text"
                          isInvalid={!input.mobileNo}
                          value={input.mobileNo}
                          onChange={e => setInput({ ...input, mobileNo: e.target.value })}
                        />
                      </FloatingLabel>
                    </Col>                    
                    <hr />
                    <Col sm={12}>
                      <FloatingLabel label="Address Line 1" className="mb-3" >
                        <Form.Control
                          type="text"
                          name="address1"
                          isInvalid={!input.address.street1}
                          value={input.address.street1}
                          onChange={e => setInput({ ...input, address: { ...input.address, street1: e.target.value } })}
                        />
                        <Form.Control.Feedback type="invalid">
                          Address Line 1 is REQUIRED
                        </Form.Control.Feedback>
                      </FloatingLabel>
                    </Col>

                    <Col sm={12}>
                      <FloatingLabel label="Address Line 2" className="mb-3" >
                        <Form.Control
                          type="text"
                          name="address2"
                          value={input.address.street2}
                          onChange={e => setInput({ ...input, address: { ...input.address, street2: e.target.value } })}
                        />
                        <Form.Text>
                          Optional
                        </Form.Text>
                      </FloatingLabel>
                    </Col>

                    <Col md={4} sm={12}>
                      <FloatingLabel label="Your City" className="mb-3" >
                        <Form.Control
                          type="text"
                          name="city"
                          isInvalid={!input.address.city}
                          value={input.address.city}
                          onChange={e => setInput({ ...input, address: { ...input.address, city: e.target.value } })}
                        />
                        <Form.Control.Feedback type="invalid">
                          City is REQUIRED
                        </Form.Control.Feedback>
                      </FloatingLabel>
                    </Col>

                    <Col md={4} sm={12}>
                      <FloatingLabel label="Your State/Province" className="mb-3" >
                        <Form.Control
                          type="text"
                          name="province"
                          isInvalid={!input.address.province}
                          value={input.address.province}
                          onChange={e => setInput({ ...input, address: { ...input.address, province: e.target.value } })}
                        />
                        <Form.Control.Feedback type="invalid">
                          State/Province is REQUIRED
                        </Form.Control.Feedback>
                      </FloatingLabel>
                    </Col>

                    <Col md={4} sm={12}>
                      <FloatingLabel label="Your Postal Code" className="mb-3" >
                        <Form.Control
                          type="number"
                          name="zip"
                          isInvalid={!input.address.postalCode}
                          value={input.address.postalCode}
                          onChange={e => setInput({ ...input, address: { ...input.address, postalCode: e.target.value } })}
                        />
                        <Form.Control.Feedback type="invalid">
                          Postal Code is REQUIRED
                        </Form.Control.Feedback>
                      </FloatingLabel>
                    </Col>

                    <Col sm={12}>
                      <FloatingLabel label="Your Country" className="mb-3" >
                        <Form.Select
                          isInvalid={!input.address.country}
                          value={input.address.country}
                          onChange={e => setInput({ ...input, address: { ...input.address, country: e.target.value } })}
                        >
                          <option>Select Your Country</option>
                          <RenderCountries />
                        </Form.Select>
                        <Form.Control.Feedback type="invalid">
                          Country is REQUIRED
                        </Form.Control.Feedback>
                      </FloatingLabel>
                    </Col>
                  </Row>
              </Card.Body>
            </Card>
          </Col>

          <Col md={3} sm={12}>
            <Card className="adminCard">
              <Card.Body>
                <Alert show={alertMessage.status} variant="danger" className="mt-3 mb-0">
                  {alertMessage.message}
                </Alert>
                <div align="center">
                  <Button
                    className="mt-3"
                    variant="primary"
                    type="submit"
                    id="submitBtn"
                    disabled={!isActive}
                    onSubmit={updateProfile}
                    size="lg"
                  >
                    <FontAwesomeIcon color="success" icon="floppy-disk" /> Update Profile
                  </Button>
                </div>                            
              </Card.Body>
            </Card>  
          </Col>
        </Row>     
        </Form>        
      }        
      </Container>
  	</>
    )
}
