import { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux'
import { useParams } from 'react-router-dom';

import { Container, Card, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { Navigate } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { AUTH_CHECK } from "@/helpers/auth-check.js"
import { API_PRIVATE } from "@/helpers/api-private.js"

import { loadingSet } from '@/store/slice/loadingSlice'

export default function HistoryInfo(props) {
    const isLoggedIn = AUTH_CHECK()
    const USER_ID = useParams();

    // const loading = useSelector((state) => state.loading)
    const dispatch = useDispatch()    

    const [data, setData] = useState({
    	purchasedOn: '',
    	items: [],
    	subtotal: ''
    })

    const dateOptions = { 
    	weekday: 'long', 
    	year: 'numeric', 
    	month: 'long',
    	day: 'numeric', 
    	hour: '2-digit',
    	minute: '2-digit'
	}

    useEffect(() => {
        dispatch( loadingSet({ status: true, message: 'Fetching Profile...' }) )
        API_PRIVATE(null, `/api/users/orders/${USER_ID.id}`, 'get')
          .then( (response) => {
            console.log(JSON.parse(response))
            setData( JSON.parse(response)[0] )
            dispatch( loadingSet({ status: false, message: '' }) )
        })
    }, [dispatch, USER_ID]);    

    function RenderItems() {
    	return(	
	    	data.items.map( (i, key) => {
	    		return (
	    			<li key={key}>
	    				({i.quantity}) 
	    				{ (i.productId !== null) ?  <Link to={`/my/promos/${i.productId}`}>{i.name}</Link> : i.name } x  <strong>{i.price}</strong>
	    				<em>{i.subtotal}</em>
	    			</li>
	    		)
	    	})
	    )	
    }

    return (
  <>
  <Container>
      { !isLoggedIn ? 
        <Navigate to="/not-logged-in" />
        :
        <Card className="brownCard flexible">
          <Card.Body>
            <Card.Title><h1 align="center">Expense Details: <span>{ new Date(data.purchasedOn).toLocaleDateString("en-US", dateOptions) }</span></h1></Card.Title>
            
            <div className="receipt">           
              <h4 align="center">Items bought</h4>
              <ul>
                <RenderItems />
              </ul>
              <h5><span>Total: {data.subtotal}</span></h5>
              <div align="right">
                <Button variant="secondary" as={Link} to="/my/history">
                  <FontAwesomeIcon color="success" icon="cart-shopping" /> Back To My Shopping History
                </Button>          
              </div>  
            </div>
          </Card.Body>
        </Card>
      }    
  </Container>
  </>
    )
}
