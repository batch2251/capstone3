import React, { useState, useEffect } from 'react';
import { Link, Navigate, useNavigate } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { useDispatch } from 'react-redux'
import { filterSet } from '@/store/slice/filterSlice'

import { Container, Row, Col, Card, Button, Form} from 'react-bootstrap';

import Months from '@/constants/constant.months'
import { AUTH_CHECK } from "@/helpers/auth-check.js"

import FeaturedCarousel from '@/components/sections/FeaturedCarousel';
import UserLineChart from '@/components/charts/UserLineChart';
import UserDoughnutChart from '@/components/charts/UserDoughnutChart';


export default function History(props) {
    const isLoggedIn = AUTH_CHECK()
    const navigate = useNavigate();
    const dispatch = useDispatch()

    //const filter = useSelector((state) => state.filter)
    const [input, setInput] = useState({month: '', year: ''})
    const [submit, setSubmit] = useState(true)   

    useEffect(() => {
        if(input.month && input.year) {
          setSubmit(false)
        } else {
          setSubmit(true)
        }
    }, [input]);    

    const RenderMonths = () => {
        return (
          Months.map( (item, index) => {
            return (
              <option value={item} key={index}>{item}</option>
            )
          })
        )
    }   

    const RenderYear = () =>  {
        let date = new Date()
        let year = date.getFullYear()      
        let items = [];
        for (let page = year; page >= (year - 5); page--) {
          items.push(
            <option key={page} value={page}>{page}</option>,
          );
        }                  
        return (
          <>{items}</>
        )      
    }       

    const updateDate = (e) => {
      e.preventDefault()
      // dispatch( loadingSet({ status: true, message: 'Updating Profile...' }) )
      dispatch( filterSet({ month: input.month, year: input.year }) )      
      navigate(`/my/history?month=${input.month}&year${input.year}`)
    }
    return (
  <>
  <Container>
      { !isLoggedIn ? 
        <Navigate to="/not-logged-in" />
        :
        <>
        <FeaturedCarousel />
        <Card className="brownCard flexible home">
          <Card.Body>
            <Card.Title>
              <h1 align="center">My Shopping History</h1>
              <Form onSubmit={(e) => updateDate(e)} id="dataFilter">
                <Row>
                  <Col md={5} sm={12}>
                    <Form.Select value={input.month} onChange={e => setInput({ ...input, month: e.target.value })}>
                      <option value="">Select Month</option>
                      <RenderMonths />
                    </Form.Select>              
                  </Col>

                  <Col md={5} sm={12}>
                    <Form.Select value={input.year} onChange={e => setInput({ ...input, year: e.target.value })}>
                      <option value="">Select Year</option>
                      <RenderYear />
                    </Form.Select>               
                  </Col>   

                  <Col md={2} sm={12}>           
                    <Button type="submit" disabled={submit}>Apply</Button>
                  </Col>
                </Row> 
              </Form>       
              <hr />     
            </Card.Title>

            <Row className="mt-4">
              <Col md={8} sm={12}>
                <UserLineChart />
              </Col>
              <Col md={4} sm={12}>
                <UserDoughnutChart />
              </Col>
            </Row>
            
            <hr />

            <p>Praesent sed nulla id nunc volutpat vestibulum eu a elit. Suspendisse interdum tempus eros, eget sollicitudin eros facilisis vel. Curabitur gravida leo lacus, eget congue justo lacinia id. Ut eleifend rhoncus tortor, at vestibulum sapien ullamcorper non. Quisque convallis nec risus congue convallis. Proin suscipit dignissim diam. Curabitur non nisl sem. Ut molestie, lorem dapibus bibendum tincidunt, metus orci sodales purus, ac porttitor dui ligula at metus. Nullam turpis ante, auctor nec nibh eget, semper elementum turpis. Vestibulum quis lorem condimentum, facilisis elit non, commodo risus. Vestibulum quis pellentesque libero. Morbi at turpis hendrerit, dictum augue vitae, vestibulum erat. Aenean erat orci, eleifend a ante at, vestibulum imperdiet velit. Praesent finibus in elit sed sagittis. Pellentesque nec erat eu nulla accumsan laoreet placerat sed tellus.</p>
            <div align="center">
            <Button variant="primary" as={Link} to="/grocery-list" size="lg">
              <FontAwesomeIcon color="success" icon="cart-shopping" /> Create A Grocery List
            </Button>
            </div>
          </Card.Body>
        </Card>        
        </>
      }

  </Container>
  </>
    )
}
