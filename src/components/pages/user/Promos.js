import { useState, useEffect } from 'react';

import { useParams } from 'react-router-dom'
import {useDispatch } from 'react-redux'
import { loadingSet } from '@/store/slice/loadingSlice'

import { Container, Card } from 'react-bootstrap';
//import { Link } from 'react-router-dom';
//import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { API_PUBLIC } from "@/helpers/api-public.js"

export default function Promos(props) {
    const PRODUCT_ID = useParams();

    //const loading = useSelector((state) => state.loading)
    const dispatch = useDispatch()    

    const [data, setData] = useState({
    	result: {
    		name: '',
    		imageUrl: '',
    		description: ''
    	}
    })

    useEffect( () => {
          dispatch( loadingSet({ status: true, message: 'Getting Product Details...' }) )
          API_PUBLIC(null, `/api/products/${PRODUCT_ID.id}`, 'get')
            .then( (response) => {
              setData( JSON.parse(response) )
              dispatch( loadingSet({ status: false, message: '' }) )
            })
    }, [dispatch, PRODUCT_ID]);

    return (
  <>
  <Container>
      <Card className="brownCard flexible home">
        <Card.Body>
          <Card.Title><h1 align="center">{data.result.name}</h1></Card.Title>
          <img src={data.result.imageUrl} alt={data.result.name} className="mb-5" />	
          {data.result.description}
          <hr />
        </Card.Body>
      </Card>
  </Container>
  </>
    )
}
