import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

import { useDispatch } from 'react-redux'
import { loadingSet } from '@/store/slice/loadingSlice'
import { toastSet } from '@/store/slice/toastSlice'

import { Card, Form, Button, FloatingLabel, Container, Row, Col, Alert } from 'react-bootstrap';

import { API_AUTH } from "@/helpers/api-public.js"

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import blankInput from '@/constants/blank.user'
import Countries from '@/constants/countries'

export default function Register() {
  const navigate = useNavigate();

  const dispatch = useDispatch()

  const [input, setInput] = useState(blankInput)
  const [isActive, setIsActive] = useState(false);

  const [alertMessage, setAlertMessage] = useState({
    status: false,
    message: ''
  })

	useEffect(() => {
		if(
      input.firstName !== "" &&
      input.lastName !== "" &&
      input.email !== "" &&
      input.password !== "" &&
      input.address.street1 !== "" &&
      input.address.city !== "" &&
      input.address.province !== "" &&
      input.address.postalCode !== "" &&
      (input.password ===  input.password2)
    )
		{
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [input]);


  const registerUser = async (e) => {
    e.preventDefault()

    dispatch( loadingSet({ status: true, message: 'Creating your Account' }) )

    let api = await API_AUTH(input, '/api/register')
      .then( (response) => {

        return JSON.parse(response)
      })


    if(api) {
      if( api.hasOwnProperty('error') ) {
        let errors = ''
        Object.entries(api.error.errors).map( (e) => {
          errors = errors + e[1].message + '. '
          return e
        })

        setAlertMessage({ status: true, message: errors })
      } else {
        dispatch( toastSet({ status: true, message: 'Registration success! You can now get full access by logging in!' }) )
        setInput(blankInput)
        navigate('/login')
      }
      dispatch( loadingSet({ status: false, message: '' }) )
    }
  }

  const RenderCountries = () => {
      return (
        Countries.map( (item, index) => {
          return (
            <option value={item.code} key={index}>{item.name}</option>
          )
        })
      )
  }
  
  return (
    <>
    <Container>
      <Card className="brownCard flexible">
        <Card.Body>
          <Card.Title><h1>Create an Account</h1></Card.Title>
          <p align="right" className="error"><strong>*All fields marked red is required.</strong></p>

          <Form onSubmit={(e) => registerUser(e)}>
            <Row>
              <Col md={6} sm={12}>
                <FloatingLabel label="Your First Name" className="mb-3" >
                  <Form.Control
                    type="text"
                    name="firstname"
                    isInvalid={!input.firstName}
                    value={input.firstName}
                    onChange={e => setInput({ ...input, firstName: e.target.value })}
                  />
                  <Form.Control.Feedback type="invalid">
                    First Name is REQUIRED
                  </Form.Control.Feedback>
                </FloatingLabel>
              </Col>
              <Col md={6} sm={12}>
                <FloatingLabel label="Your Last Name" className="mb-3" >
                  <Form.Control
                    type="text"
                    name="lastname"
                    isInvalid={!input.lastName}
                    value={input.lastName}
                    onChange={e => setInput({ ...input, lastName: e.target.value })}
                  />
                  <Form.Control.Feedback type="invalid">
                    Last Name is REQUIRED
                  </Form.Control.Feedback>
                </FloatingLabel>
              </Col>

              <Col md={6} sm={12}>
                <FloatingLabel label="Your Email" className="mb-3" >
                  <Form.Control
                    type="email"
                    isInvalid={!input.email || !(/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(input.email)) }
                    value={input.email}
                    onChange={e => setInput({ ...input, email: e.target.value })}
                  />
                  <Form.Control.Feedback type="invalid">
                    Valid Email is REQUIRED
                  </Form.Control.Feedback>
                </FloatingLabel>
              </Col>

              <Col md={6} sm={12}>
                <FloatingLabel label="Your Mobile #" className="mb-3" >
                  <Form.Control
                    type="text"
                    name="phone"
                    value={input.mobileNo}
                    onChange={e => setInput({ ...input, mobileNo: e.target.value })}
                  />
                  <Form.Control.Feedback>
                    Optional
                  </Form.Control.Feedback>
                </FloatingLabel>
              </Col>

              <Col md={6} sm={12}>
                <FloatingLabel label="Your Password" className="mb-3" >
                  <Form.Control
                    type="password"
                    isInvalid={!input.password || input.password.length < 8}
                    value={input.password}
                    onChange={e => setInput({ ...input, password: e.target.value })}
                  />
                  <Form.Control.Feedback type="invalid">
                    Password must be greater than 8 characters
                  </Form.Control.Feedback>
                </FloatingLabel>
              </Col>

              <Col md={6} sm={12}>
                <FloatingLabel label="Confirm Your Password" className="mb-3" >
                  <Form.Control
                    type="password"
                    isInvalid={ !input.password2 || (input.password !== input.password2)}
                    value={input.password2}
                    onChange={e => setInput({ ...input, password2: e.target.value })}
                  />
                  <Form.Control.Feedback type="invalid">
                    Passwords does not match
                  </Form.Control.Feedback>
                </FloatingLabel>
              </Col>

              <Col sm={12}>
                <FloatingLabel label="Address Line 1" className="mb-3" >
                  <Form.Control
                    type="text"
                    name="address1"
                    isInvalid={!input.address.street1}
                    value={input.address.street1}
                    onChange={e => setInput({ ...input, address: { ...input.address, street1: e.target.value } })}
                  />
                  <Form.Control.Feedback type="invalid">
                    Address Line 1 is REQUIRED
                  </Form.Control.Feedback>
                </FloatingLabel>
              </Col>

              <Col sm={12}>
                <FloatingLabel label="Address Line 2" className="mb-3" >
                  <Form.Control
                    type="text"
                    name="address2"
                    value={input.address.street2}
                    onChange={e => setInput({ ...input, address: { ...input.address, street2: e.target.value } })}
                  />
                  <Form.Text>
                    Optional
                  </Form.Text>
                </FloatingLabel>
              </Col>

              <Col md={4} sm={12}>
                <FloatingLabel label="Your City" className="mb-3" >
                  <Form.Control
                    type="text"
                    name="city"
                    isInvalid={!input.address.city}
                    value={input.address.city}
                    onChange={e => setInput({ ...input, address: { ...input.address, city: e.target.value } })}
                  />
                  <Form.Control.Feedback type="invalid">
                    City is REQUIRED
                  </Form.Control.Feedback>
                </FloatingLabel>
              </Col>

              <Col md={4} sm={12}>
                <FloatingLabel label="Your State/Province" className="mb-3" >
                  <Form.Control
                    type="text"
                    name="province"
                    isInvalid={!input.address.province}
                    value={input.address.province}
                    onChange={e => setInput({ ...input, address: { ...input.address, province: e.target.value } })}
                  />
                  <Form.Control.Feedback type="invalid">
                    State/Province is REQUIRED
                  </Form.Control.Feedback>
                </FloatingLabel>
              </Col>

              <Col md={4} sm={12}>
                <FloatingLabel label="Your Postal Code" className="mb-3" >
                  <Form.Control
                    type="number"
                    name="zip"
                    isInvalid={!input.address.postalCode}
                    value={input.address.postalCode}
                    onChange={e => setInput({ ...input, address: { ...input.address, postalCode: e.target.value } })}
                  />
                  <Form.Control.Feedback type="invalid">
                    Postal Code is REQUIRED
                  </Form.Control.Feedback>
                </FloatingLabel>
              </Col>

              <Col sm={12}>
                <FloatingLabel label="Your Country" className="mb-3" >
                  <Form.Select
                    isInvalid={!input.address.country}
                    value={input.address.country}
                    onChange={e => setInput({ ...input, address: { ...input.address, country: e.target.value } })}
                  >
                    <option>Select Your Country</option>
                    <RenderCountries />
                  </Form.Select>
                  <Form.Control.Feedback type="invalid">
                    Country is REQUIRED
                  </Form.Control.Feedback>
                </FloatingLabel>
              </Col>
            </Row>

            <Alert show={alertMessage.status} variant="danger" className="mt-3 mb-0">
              <>
              {alertMessage.message}
              </>
            </Alert>

            <hr />

            <div align="center">
              <Button
                className="mt-3"
                variant="primary"
                type="submit"
                id="submitBtn"
                onSubmit={registerUser}
                disabled={!isActive}
                size="lg"
                >
                  <FontAwesomeIcon color="success" icon="floppy-disk" /> Create My Account!
              </Button>
            </div>
          </Form>
        </Card.Body>
      </Card>
    </Container>
    </>
    )

}
