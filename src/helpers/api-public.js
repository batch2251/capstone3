import axios from 'axios'

export function API_AUTH(data, url) {
  return axios({
    method: 'post',
    responseType: 'application/json',
    url: process.env.REACT_APP_API_ROOT + url,
    data: data
  })
   .then(response => {
      if (response.status === 200) {
        return response.data
      }
   })
   .catch( error => {
      if (error.response) {
        return { error: `${error.response.data} || ${error.response.status} || ${error.response.headers}` }
      } else if (error.request) {
        return { error: error.request}
      } else {
        return { error: error.message }
      }
      // return Promise.reject(error);
   })
}

export function API_PUBLIC(data, url, method) {
  return axios({
    method: method,
    responseType: 'application/json',
    url: process.env.REACT_APP_API_ROOT + url,
    data: data
  })
   .then(response => {
      if (response.status === 200) {
        return response.data
      }
   })
   .catch( error => {
      if (error.response) {
        return { error: `${error.response.data} || ${error.response.status} || ${error.response.headers}` }
      } else if (error.request) {
        return { error: error.request}
      } else {
        return { error: error.message }
      }
      // return Promise.reject(error);
   })
}
