import axios from 'axios'

export function API_PRIVATE(data, url, method) {
  let token = (localStorage.getItem('listahan_user') === null ) ? null : JSON.parse(localStorage.getItem('listahan_user')).access
  return axios({
    method: method,
    responseType: 'application/json',
    url: process.env.REACT_APP_API_ROOT + url,
    headers: { 'Authorization': 'Bearer ' + token },
    data: data
  })
   .then(response => {
      if (response.status === 200) {
        return response.data
      }
   })
   .catch( error => {
      if (error.response) {
        return { error: `${error.response.data} || ${error.response.status} || ${error.response.headers}` }
      } else if (error.request) {
        return { error: error.request}
      } else {
        return { error: error.message }
      }
      // return Promise.reject(error);
   })
}
