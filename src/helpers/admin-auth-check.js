export function ADMIN_AUTH_CHECK() {
  const storage = localStorage.getItem('listahan_user')

  if (  storage !== null && JSON.parse(storage).role === 0 ) {
    document.body.classList.add('admin')
    return true
  } else {
    document.body.classList.remove('admin')
    return false
  }
}
